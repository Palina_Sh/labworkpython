## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


##Task's tracker.

This application is intended for the user to maintain calendar lists of tasks. The application consists of two parts:

1. Library. Contains all the necessary functions for working with tasks: adding, deleting, editing, linking, notifying users, logging actions, saving to the database.  
You can use the library by importing it separately: import logic.

2. The console part. Uses functions from the library. It is designed to work in the command line. The main purpose is data validation before using library functions.  
There are configuration files: patways.conf and logging.conf. These files contain settings: paths to the database and archive files, format, path, level and status (enabled, disabled) of the log.


The program runs from the command line. To get started, you need to install the application:

1. Move to folder ../project.

2. Before installing the application directly, you can change the default logging path and database:  
    python3 setup.py set_paths

3. Installing the application:  
    python3 setup.py install

4. To work with the autocompletion of commands, you must install the package argcomplete:  
    pip install argcomplete  
    eval "$(register-python-argcomplete tracker)"

5. Enter tracker <command> [actions] [<keys>]

P.s. paragraphs 2 and 3 can be combined:  
    python3 setup.py set_paths install


In order to call the tests from the modules tests_for_logic from the command line, it is necessary to assign: python3 setup.py test


To view all the commands, we enter help: tracker -h

Available commands: user (actions={add}), task (actions={add, remove, edit}), link (actions={add, remove}), show (actions={task, tasks, link}), logger, config (actions={logging, archive, tasks}).


Main commands:

1. To add a task (specify the keys necessary to add a particular task (all information about them: tracker task add -h). Required is the -u USER keys-the user to whom the new task is added.):  
    tracker task add -u USER [-n NAME] [-type TYPE] ...

2. To edit a task (specify the keys which fields we want to change for the task (all information about them: tracker task edit -h). Required: -u USER and - ID ID - which user is editing the task and what task.):  
    tracker task edit -u USER --ID ID [-n NAME] ...

3. To remove a task (TASK - task's ID, which USER want to remove.):  
    tracker task remove --t TASK -u USER

4. To add a new user to a task (USER - new user in task with ID = TASK.):  
    tracker user add -u USER -t TASK

5. To add link between two tasks (FIRST and SECOND - ID linked tasks.):  
    tracker link add -f FIRST -s SECOND

6. To remove link between ywo tasks (FIRST and SECOND - tasks's ID, between which removed link.):  
    tracker link remove -f FIRST -s SECOND

7. View task information:  
    tracker show task ID

8. View tasks information (P.s. key --sort (-s) sorts tasks on priority or type, and key --day (-d) prints tasks desired day.):  
    about main user's tasks:  
        tracker show tasks -u USER

    about all user's tasks:  
        tracker show tasks -u USER -a

    about task's subtasks:  
        tracker show tasks -t TASK

9. View linked tasks:  
    tracker show link TASK

10. Enable or disable logging:  
    tracker logger [--on] [--off]

11. Changing paths to log files, archiving, task database:  
    tracker config logging PATH  
    tracker config archive PATH  
    tracker config tasks PATH  
