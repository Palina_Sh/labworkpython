import argparse
import argcomplete
from command_line import handler
from task_tracker import logger


def create_parser():
    parser = argparse.ArgumentParser(description="This application is intended for the user to maintain calendar lists "
                                                 "of cases and tasks. For work enter "
                                                 "'tracker <command> [actions] [<keys>]'.")
    subparsers = parser.add_subparsers(dest='command', help="Choose command for begin work", metavar='')

    user_parser = subparsers.add_parser('user', help="Command includes actions for work with users")
    user_subparsers = user_parser.add_subparsers(dest='action', metavar="")
    create_user_parser(user_subparsers)

    category_parser = subparsers.add_parser('category', help="Command includes actions for work with categories")
    category_subparsers = category_parser.add_subparsers(dest='action', metavar="")
    create_category_parser(category_subparsers)

    task_parser = subparsers.add_parser('task', help="Command for work with tasks")
    task_subparsers = task_parser.add_subparsers(dest='action', metavar="")
    create_task_parser(task_subparsers)

    link_parser = subparsers.add_parser('link', help="Command for work with task's links")
    link_subparsers = link_parser.add_subparsers(dest='action', metavar="")
    create_link_parser(link_subparsers)

    view_parser = subparsers.add_parser('show', help="Command includes actions for showing data")
    view_subparsers = view_parser.add_subparsers(dest='action', metavar="")
    create_view_parser(view_subparsers)

    logging = subparsers.add_parser("logger", help="Command for setting, turned on or turned off logging.")
    logging.add_argument('--on', action='store_const', const=True, help="Turned on logger")
    logging.add_argument('--off', action='store_const', const=True, help="Turned off logger")
    logging.add_argument('--format', '-f', help="format of the log line")
    logging.add_argument('--path', '-p', help="path to the file in which the logs are written")
    logging.add_argument('--level', '-l', help="with what level to log",
                         choices={"DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"})

    config_parser = subparsers.add_parser("config", help="Command for setting paths to archive and database")
    config_subparsers = config_parser.add_subparsers(dest='action', metavar="")
    create_config_parser(config_subparsers)

    return parser


def create_user_parser(user_subparsers):

    add_user = user_subparsers.add_parser('add', help="Adding user in group (to some task)")
    add_user.add_argument('--user', '-u', required=True, default="", help="Username to which you want to add task")
    add_user.add_argument('--task', '-t', required=True, default="", help="Task's ID")

    remove_user = user_subparsers.add_parser('remove', help="Removing user from group")
    remove_user.add_argument('--user', '-u', required=True, default="", help="Username, which you want "
                                                                             "to remove from task")
    remove_user.add_argument('--task', '-t', required=True, default="", help="Task's ID")


def create_category_parser(category_subparsers):

    add_category = category_subparsers.add_parser('add', help="Adding new category in storage")
    add_category.add_argument('--user', '-u', required=True, default="", help="Username, which want to add category")
    add_category.add_argument('--name', '-n', required=True, default="", help="Category's name")

    remove_category = category_subparsers.add_parser('remove', help="Removing category")
    remove_category.add_argument('--user', '-u', required=True, default="", help="Username, which want "
                                                                                 "to remove some category")
    remove_category.add_argument('--ID', '-i', required=True, default="", help="Category's ID")

    edit_category = category_subparsers.add_parser('edit', help="Edited category's name")
    edit_category.add_argument('--name', '-n', required=True, help="New category's name")
    edit_category.add_argument('--ID', '-i', required=True, help="Category's ID, which you want to edit")
    edit_category.add_argument('--user', '-u', required=True, help="User. which wants to edit this category")


def create_task_parser(task_subparsers):

    add_task_in_user = task_subparsers.add_parser('add', help="Command for adding new tasks or subtasks")
    add_task_in_user.add_argument('--user', '-u', required=True, default="",
                                  help="Username to which you want to add new task")
    add_task_in_user.add_argument('--parent', default="", help="Task's ID to which you want to add new subtask")
    add_task_in_user.add_argument('--name', '-n', default="", help="Task's name")
    add_task_in_user.add_argument('--begin', '-b', nargs='*', help="Date of task's begin in format DD-MM-YYYY HH:mm:SS")
    add_task_in_user.add_argument('--end', '-e', nargs='*', help="Date of task's end in format DD-MM-YYYY HH:mm:SS")
    add_task_in_user.add_argument('--type', '-t', default="", help="Task's type",
                                  choices=['work', 'study', 'relaxation', 'sport', 'food', 'family', 'reading',
                                           'travel', 'meeting', 'event'])
    add_task_in_user.add_argument('--priority', '-p', choices=[1, 2, 3, 4, 5], default=2, type=int,
                                  help="Task's priority (1 - low, 3 - medium, 5 - high).")
    add_task_in_user.add_argument('--status', '-s', choices=['process', 'done', 'wait'], default="process",
                                  help="Task's status")
    add_task_in_user.add_argument('--comment', '-c', default="", help="Task's comment")
    add_task_in_user.add_argument('--reminder', '-r', nargs='*', help="Task's reminder in format 'at HH:SS comment' \
                                                                      or 'TIME m/h/d ago comment'")
    add_task_in_user.add_argument('--repeat', nargs='*', help="Creates period task with rule: weekends/weekdays or\
                                                               every _ minutes/hours/days/weeks/months")
    add_task_in_user.add_argument('--plan', nargs='*', help="Creates task, which creates on plan:\
                                                             every _ minutes/hours/days/weeks/months")
    add_task_in_user.add_argument('--category', help="Category's ID, to which the task belongs")

    remove_task = task_subparsers.add_parser('remove', help="Command for removing task")
    remove_task.add_argument('ID', help=" ID removed task")

    edit_task = task_subparsers.add_parser('edit', help="Command for editing tasks")
    edit_task.add_argument('--ID', required=True, help="Task's or subtask's ID")
    edit_task.add_argument('--user', '-u', required=True, help="User's name, which edits task")
    edit_task.add_argument('--name', '-n', help="New task's name")
    edit_task.add_argument('--begin', '-b', nargs='*', help="Date of task's begin in format DD-MM-YYYY HH:mm:SS")
    edit_task.add_argument('--end', '-e', nargs='*', help="Date of task's end in format DD-MM-YYYY HH:mm:SS")
    edit_task.add_argument('--type', '-t', help="New task's type",
                           choices=['work', 'study', 'relaxation', 'sport', 'food', 'family', 'reading',
                                    'travel', 'meeting', 'event'])
    edit_task.add_argument('--priority', '-p', choices=[1, 2, 3, 4, 5], type=int, help="New task's priority (1 - low, "
                                                                                       "3 - medium, 5 - high).")
    edit_task.add_argument('--status', '-s', choices=['process', 'done', 'fail', 'wait'], help="New task's status")
    edit_task.add_argument('--comment', '-c', help="New task's comment")
    edit_task.add_argument('--reminder', '-r', nargs='*', help="Task's reminder in format 'at HH:SS comment' or \
                                                               'TIME m/h/d ago comment'")
    edit_task.add_argument('--repeat', nargs='*', default="", help="Creates period task with rule: weekends/weekdays or\
                                                                    every _ minutes/hours/days/weeks/months")
    edit_task.add_argument('--plan', nargs='*', help="Creates task, which creates on plan:\
                                                             every _ minutes/hours/days/weeks/months")
    edit_task.add_argument('--category', help="New category's ID, to which the task belongs")

    search_task = task_subparsers.add_parser('search', help="Command for searching tasks on some fields")
    search_task.add_argument('--name', '-n', help="Task's name")
    search_task.add_argument('--begin', '-b', help="Task's start date")
    search_task.add_argument('--end', '-e', help="Task's end date")
    search_task.add_argument('--create', '-c', help="Task's create date")
    search_task.add_argument('--priority', '-p', help="Task's priority")
    search_task.add_argument('--type', '-t', help="Task's type")


def create_link_parser(link_subparsers):

    add_link = link_subparsers.add_parser('add', help="Command adds new link between two tasks")
    add_link.add_argument('--first', '-f', required=True, help="First task's ID")
    add_link.add_argument('--second', '-s', required=True, help="Second task's ID")

    remove_link = link_subparsers.add_parser('remove', help="Command deletes link between two tasks")
    remove_link.add_argument('--first', '-f', required=True, help="First task's ID")
    remove_link.add_argument('--second', '-s', required=True, help="Second task's ID")


def create_view_parser(view_subparsers):

    view_task = view_subparsers.add_parser('task', help="Allows you to see main information about the task")
    view_task.add_argument('ID', help="Task's ID")

    view_tasks = view_subparsers.add_parser('tasks', help="Command for viewing user's tasks. If you want to see main \
                                                          user's tasks, you must enter username without task's ID. If \
                                                          you want to see some task's subtasks you must enter some \
                                                          task's ID without username")
    view_tasks.add_argument('--user', '-u', help="User's name")
    view_tasks.add_argument('--name', '-n', help="Task's name")
    view_tasks.add_argument('--all', '-a', action='store_const', const=True, help="Outputs not only main tasks (those, \
                                                                                  that do not have subtasks), but all")
    view_tasks.add_argument('--task', '-t', help="Task's ID, for which you want to see subtasks")
    view_tasks.add_argument('--day', '-d', help="Views tasks of the date DD-MM-YYYY or DD/MM/YYYY")
    view_tasks.add_argument('--sort', '-s', choices=['type', 'priority'], help="Prints sorted tasks")

    view_linked_tasks = view_subparsers.add_parser('link', help="Command for viewing linked tasks")
    view_linked_tasks.add_argument('task', help="Task's ID for which you want to print siblings")

    view_categories = view_subparsers.add_parser('users', help="Command for showing all users")
    view_categories.add_argument('--all', '-a', action='store_const', const=True, help="Showing all users in application")

    view_categories = view_subparsers.add_parser('categories', help="Command for showing all categories")
    view_categories.add_argument('--all', '-a', action='store_const', const=True, help="Showing all users's ategories "
                                                                                       "or all categories in storage")
    view_categories.add_argument('--user', '-u', help="Username, which wants to see all its categories.")


def create_config_parser(config_subparsers):

    archive = config_subparsers.add_parser('archive', help="Set path to removed tasks")
    archive.add_argument('path', help="Path to archive file")

    tasks = config_subparsers.add_parser('tasks', help="Changed path to database")
    tasks.add_argument('path', help="Path to tasks file")

    categories = config_subparsers.add_parser('categories', help="Set path to file with categories")
    categories.add_argument('path', help="Path to file")

    relations = config_subparsers.add_parser('relations', help="Changes path to file with relations")
    relations.add_argument('path', help="Path to file")


def select_action(parser):
    namespace = parser.parse_args()
    if namespace.command == "user":
        if namespace.action == "add":
            handler.add_user_in_group(namespace)
        elif namespace.action == "remove":
            handler.remove_user_from_task(namespace)
        else:
            parser._subparsers._group_actions[0].choices['user'].print_help()
    elif namespace.command == "category":
        if namespace.action == "add":
            handler.add_category(namespace)
        elif namespace.action == "remove":
            handler.remove_category(namespace)
        elif namespace.action == "edit":
            handler.edit_category(namespace)
        else:
            parser._subparsers._group_actions[0].choices['category'].print_help()
    elif namespace.command == "task":
        if namespace.action == "add":
            handler.add_new_task(namespace)
        elif namespace.action == "remove":
            handler.remove_task(namespace)
        elif namespace.action == "edit":
            handler.edit_task(namespace)
        elif namespace.action == "search":
            handler.search_tasks(namespace)
        else:
            parser._subparsers._group_actions[0].choices['task'].print_help()
    elif namespace.command == "link":
        if namespace.action == "add":
            handler.add_link(namespace)
        elif namespace.action == "remove":
            handler.remove_link(namespace)
        else:
            parser._subparsers._group_actions[0].choices['link'].print_help()
    elif namespace.command == "show":
        if namespace.action == "task":
            handler.print_task(namespace)
        elif namespace.action == "tasks":
            handler.show_tasks(namespace)
        elif namespace.action == "link":
            handler.print_linked_task(namespace)
        elif namespace.action == "users":
            handler.show_users()
        elif namespace.action == "categories":
            handler.show_categories(namespace)
        else:
            parser._subparsers._group_actions[0].choices['show'].print_help()
    elif namespace.command == "logger":
        handler.configure_logging(namespace)
    elif namespace.command == "config":
        if namespace.action == "archive":
            handler.change_archive_path(namespace.path)
        elif namespace.action == "tasks":
            handler.change_tasks_path(namespace.path)
        elif namespace.action == "categories":
            handler.change_categories_path(namespace.path)
        elif namespace.action == "relations":
            handler.change_relations_path(namespace.path)
        else:
            parser._subparsers._group_actions[0].choices['config'].print_help()
    else:
        parser.print_help()


def main():
    logger.create_logger(handler.get_logging_enable(), handler.get_logging_level(),
                         handler.get_logging_path(), handler.get_logging_format())

    parser = create_parser()
    argcomplete.autocomplete(parser)  # autocomplete for bash
    select_action(parser)
    handler.check()
    handler.print_notices()
