import os


LOGGING_CONF = os.path.dirname(__file__) + "/configurations/logging.conf"
PATHWAYS_CONF = os.path.dirname(__file__) + "/configurations/pathways.conf"
DEFAULT_DATABASE_PATH = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/Tasks.json"
DEFAULT_ARCHIVE_PATH = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/RemovedTasks.json"
DEFAULT_CATEGORIES_PATH = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/Categories.json"
DEFAULT_RELATIONS_PATH = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/Relations.json"
