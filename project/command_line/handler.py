"""The module is designed to work with the command line.

The module uses the library to create links between tasks, pre-checks all user input for correctness.

"""

import datetime
import re
import os
import sys
import configparser
import logging
from task_tracker.actions import Actions
from task_tracker.storage import Storage
from task_tracker.entities import (
    Task,
    Reminder,
    Category,
    Notice
)
from command_line import config


def create_task(namespace):
    """Creates task. Verifies the correctness of the user-entered rule for repetition of the task,
    reminders of the task, the dates of the beginning and the end of the task, the existence of the task's parent

    :param namespace = parser.parse_args()
    :return: new Task
    """

    repeat = namespace.repeat
    begin = namespace.begin
    end = namespace.end
    reminder = namespace.reminder
    plan = namespace.plan

    if namespace.repeat:
        repeat = check_repeat(namespace.repeat)
        if not repeat:
            return

    if namespace.begin:
        begin = create_begin_or_end(namespace.begin)
        if not begin:
            return
        if begin > datetime.datetime.now():
            namespace.status = "wait"

    if namespace.end:
        end = create_begin_or_end(namespace.end)
        if not end:
            return

    if namespace.reminder:
        reminder = create_reminder(namespace)
        if not reminder:
            return

    if namespace.plan:
        plan = check_repeat(namespace.plan)
        if not plan:
            return

    if is_right_parent(namespace.parent, namespace.user, begin, end) and check_category(namespace.category):
        return Task(users=[namespace.user], name=namespace.name, repeat=repeat, begin=begin, end=end,
                    type_=namespace.type, status=namespace.status, priority=namespace.priority, reminder=reminder,
                    comment=namespace.comment, parent=namespace.parent, plan=plan, category_id=namespace.category)


def create_reminder(namespace, begin=None):
    """Creates a new reminder if the user-entered string was valid, otherwise it notifies the user of an error.

    :param namespace = parser.parse_args()
    :param begin: task's start date (datetime)
    :return: new Reminder
    """

    reminder = parse_reminder(namespace, begin)
    if not reminder:
        sys.stderr.write("Reminder format is wrong. Need string 'at Hours:Minutes comment_reminder' or "
                         "'TIME months/hours/days before'. E.g. 'at 20:30 my reminder one', "
                         "'3 hours before my reminder two'.\n")
        return
    return Reminder(date=reminder[0], comment=reminder[1])


def check_repeat(repeat):
    """ Verifies the correctness of the task's repetition rule, in case of an error, notifies the user, if it succeeds,
    returns the received string.

    :param repeat - parser.parse_args() - namespace.repeat or namespace.plan
    :return: repeat
    """
    if len(repeat) >= 3 and repeat[0] == "every":
        dict_repeat = {"months": 0, "weeks": 0, "days": 0, "hours": 0, "minutes": 0}
        time = 0
        for i in range(1, len(repeat)):
            if repeat[i] in dict_repeat.keys():
                dict_repeat[repeat[i]] += time
            elif re.match(r"^[0-9]+$", repeat[i]):  # Whether the string is an integer
                time = int(repeat[i])
            else:
                break
        else:  # Creates string months-weeks-days-hours-minutes
            repeat_str = ""
            for word in repeat:
                repeat_str += word
            return repeat_str
    elif len(repeat) == 1 and (repeat[0] == "weekends" or repeat[0] == "weekdays"):
        return repeat[0]
    sys.stderr.write("Repeat is wrong. Need string 'every TIME minutes' or 'every TIME hours' or 'every TIME days' "
                     "or 'every TIME weeks' or combination this: 'every TIME hours TIME minutes' or 'weekends' or "
                     "'weekdays' or nothing. E.g. 'every 3 hours', 'every 30 days'.\n")


def check_category(category_id):
    if not category_id:
        return True
    storage = create_storage()
    category = storage.load_category(category_id)
    if not category:
        sys.stderr.write("Category with this ID is not in storage")
        return False
    storage.save_category(category)
    return True


def create_begin_or_end(begin_or_end):
    """Checks for the correct date of the beginning or the end of the task, in case of an error, notifies the user,
    in case of success returns the received date

    :param begin_or_end: string with date begin or end.
    :return: datetime task's start or end.
    """
    if not begin_or_end:
        return
    date_str = ""
    for piece in begin_or_end:
        date_str += piece + " "
    date_str = date_str.strip()
    date = parse_date(date_str)
    if not date:
        sys.stderr.write(date_str + " is wrong. Need string 'Days-Months-Years Hours:Minutes:Seconds' or "
                                    "'Days/Months/Years'. E.g. '12-07-2018 12:20', '12/07/2018'.\n")
    return date


def parse_date(date_str):
    """Gets the date in the case of the correct string format date_str.

    :param date_str: string with date.
    :return: datetime
    """
    try:
        return datetime.datetime.strptime(date_str, "%d-%m-%Y %H:%M:%S")
    except ValueError:
        pass
    try:
        return datetime.datetime.strptime(date_str, "%d/%m/%Y %H:%M:%S")
    except ValueError:
        pass
    try:
        return datetime.datetime.strptime(date_str, "%d-%m-%Y %H:%M")
    except ValueError:
        pass
    try:
        return datetime.datetime.strptime(date_str, "%d/%m/%Y %H:%M")
    except ValueError:
        pass
    try:
        return datetime.datetime.strptime(date_str, "%d-%m-%Y")
    except ValueError:
        pass
    try:
        return datetime.datetime.strptime(date_str, "%d/%m/%Y")
    except ValueError:
        pass


def parse_reminder(namespace, begin=None):
    """Checks for the correctness of the user-entered reminder string about the task.

    :param namespace = parser.parse_args()
    :param begin: task's start date
    :return: [date, comment] - list, in which first argument - date of reminder, second - reminder's comment.
    """
    if not begin:
        begin = namespace.begin
        if not namespace.begin:
            begin = datetime.datetime.combine(datetime.datetime.now().date(), datetime.time(0, 0, 0))

    reminder = namespace.reminder
    if not reminder:
        return

    comment = ""
    if len(reminder) >= 2 and reminder[0] == "at":  # Parse strings in format 'at hours:minutes' in current day
        time_str = reminder[1]
        try:
            time = datetime.datetime.strptime(time_str, "%H:%M")
        except ValueError:
            return
        date = datetime.datetime.combine(begin.date(), time.time())
        for index in range(2, len(reminder)):  # Remaining words - reminder's comment
            comment += str(reminder[index]) + " "
        return [date, comment]
    elif len(reminder) >= 3 and reminder[2] == "before":  # Parse strings in format '3 minutes/hours/days before'
        try:
            if reminder[1] == "minutes":
                date = begin - datetime.timedelta(minutes=int(reminder[0]))
            elif reminder[1] == "hours":
                date = begin - datetime.timedelta(hours=int(reminder[0]))
            elif reminder[1] == "days":
                date = begin - datetime.timedelta(days=int(reminder[0]))
            else:
                return
        except ValueError:
            return
        for index in range(3, len(reminder)):  # Remaining words - reminder's comment
            comment += str(reminder[index]) + " "
        return [date, comment]


def is_right_parent(parent, username, begin, end):
    """If the task with ID=parent exists, it returns TRUE.
    If the user username does not have access to the task with ID==parent, the end of the parent task before the end
    of the created task, and the start later, it returns FALSE.

    :param parent: task's parent (ID this task - string)
    :param username: task's user
    :param begin: task's start date
    :param end: task's end date
    :return: bool
    """
    if not parent:
        return True
    actions = Actions(create_storage())
    parent_task = actions.get_task(parent)
    if not parent_task:
        sys.stderr.write("Parent is wrong. In storage is not task with this ID.\n")
        return False
    if username not in parent_task.users:
        sys.stderr.write("User - " + username + " has not task with ID: " + parent_task.ID + ".\n")
        return False
    if begin and begin < parent_task.begin:
        sys.stderr.write("Beginning of the parent task later than the beginning of subtask.\n")
        return False
    if end and end > parent_task.end:
        sys.stderr.write("Ending of the parent task before than the ending of subtask.\n")
        return False
    return True


def create_storage():
    database_path = get_database_file()
    archive_path = get_archive_file()
    category_path = get_category_file()
    relation_path = get_relations_file()
    return Storage(database_path, archive_path, category_path, relation_path)


def add_user_in_group(namespace):
    """Adds a new user to the task or in case of failure, notifies the user about the error.

    :param namespace = parse.parse_args()
    """
    try:
        actions = Actions(create_storage())
        task = actions.add_new_user_in_task(namespace.task, namespace.user)
        if not task:
            sys.stderr.write("Task's ID is wrong. In storage is not task with this ID.\n")
            return
        print("User " + namespace.user + " is added to new group.")
    except AttributeError:
        sys.stderr.write("This user already has access to the task.\n")


def remove_user_from_task(namespace):
    """Deletes user from task's list of users or in case of failure, notices the user about error

    :param namespace = parse.parse_args()
    """
    try:
        actions = Actions(create_storage())
        task = actions.remove_user_from_task(namespace.task, namespace.user)
        if not task:
            sys.stderr.write("Task's ID is wrong. In storage is not task with this ID.\n")
            return
        print("User " + namespace.user + " is removed from group.")
    except AttributeError:
        sys.stderr.write("This user doesn't have access to task.\n")


def add_new_task(namespace):
    """Creates and adds a new task to the storage, or if the task is not created, notifies the user about it.

    :param namespace = parse.parse_args()
    """
    task = create_task(namespace)
    if not task:
        sys.stderr.write("Please try again\n")
        return
    actions = Actions(create_storage())
    new_task = actions.add_new_task(task)
    print("Added new task:", new_task)


def remove_task(namespace):
    """Deletes a task or prints an error message.

    :param namespace = parse.parse_args()
    """
    actions = Actions(create_storage())
    task = actions.remove_task(namespace.ID)
    if not task:
        sys.stderr.write("There isn't task with this ID. Please try again.\n")
        return
    print("Removed task: ", task)


def edit_task(namespace):
    """Edits those fields of the task that the user specified, pre-checks the correctness of the entered data.
    In case of failure, notify the user of an error.

    :param namespace = parse.parse_args()
    """
    actions = Actions(create_storage())
    edit = actions.get_task(namespace.ID)
    if not edit:
        sys.stderr.write("In storage isn't task with ID: " + namespace.ID + ". Please try again.\n")
        return

    if namespace.name:
        edit.name = namespace.name

    if namespace.begin:
        begin = create_begin_or_end(namespace.begin)
        if not begin:
            return
        edit.begin = begin

    if namespace.end:
        end = create_begin_or_end(namespace.end)
        if not end:
            return
        edit.begin = end

    if namespace.type:
        edit.type = namespace.type

    if namespace.priority:
        edit.priority = namespace.priority

    if namespace.status:
        edit.status = namespace.status

    if namespace.comment:
        edit.comment = namespace.comment

    if namespace.reminder:
        reminder = create_reminder(namespace, edit.begin)
        if not reminder:
            return
        edit.reminder = reminder

    if namespace.repeat:
        repeat = check_repeat(namespace.repeat)
        if not repeat:
            return
        edit.repeat = repeat

    if namespace.plan:
        plan = check_repeat(namespace.plan)
        if not plan:
            return
        edit.plan = plan

    if namespace.category and check_category(namespace.category):
        edit.category_id = namespace.category

    edit.date_edit = datetime.datetime.now()

    try:
        edited = actions.edit_task(edit, namespace.user)
        if not edited:
            sys.stderr.write("There isn't task with this ID. Please try again.\n")
            return
        print("Edited task: ", edited)
    except AttributeError:
        sys.stderr.write("This user doesn't have access to the task.\n")


def add_link(namespace):
    """Adds a link between the specified tasks or prints an error message.

    :param namespace = parser.parse-args()
    """
    try:
        actions = Actions(create_storage())
        relation = actions.add_new_link(namespace.first, namespace.second)
        if not relation:
            sys.stderr.write("There isn't task with this ID. Please try again.\n")
            return
        print("Tasks linked")
    except AttributeError:
        sys.stderr.write("These tasks are already connected.\n")


def remove_link(namespace):
    """Removes a link between the specified tasks or prints an error message.

    :param namespace = parser.parse_args()
    """
    try:
        actions = Actions(create_storage())
        relation = actions.remove_link(namespace.first, namespace.second)
        if not relation:
            sys.stderr.write("There isn't task with this ID. Please try again.\n")
            return
        print("Tasks are not connected.\n")
    except AttributeError:
        sys.stderr.write("The tasks are not related.\n")


def add_category(namespace):
    """Added new category in storage"""
    actions = Actions(create_storage())
    category = Category(namespace.user, namespace.name)
    category = actions.add_category(category)
    print("Added category: " + str(category))


def remove_category(namespace):
    """Removed category from storage with some ID"""
    actions = Actions(create_storage())
    try:
        removed_category = actions.remove_category(namespace.ID, namespace.user)
        if not removed_category:
            sys.stderr.write("Category is not found in storage\n")
            return
        print("Removed category: " + str(removed_category))
    except AttributeError:
        sys.stderr.write("This user doesn't have this category\n")


def edit_category(namespace):
    actions = Actions(create_storage())
    try:
        edited_category = actions.edit_category(namespace.ID, namespace.user, namespace.name)
        if not edited_category:
            sys.stderr.write("Category is not found in storage\n")
            return
        print("Edited category: " + str(edited_category))
    except AttributeError:
        sys.stderr.write("This user doesn't have this category\n")


def search_tasks(namespace):
    actions = Actions(create_storage())
    tasks = None
    if namespace.name:
        tasks = actions.search_name_tasks(namespace.name)
    elif namespace.begin:
        begin = create_begin_or_end(namespace.begin)
        if not begin:
            return
        tasks = actions.search_begin_task(begin)
    elif namespace.end:
        end = create_begin_or_end(namespace.end)
        if not end:
            return
        tasks = actions.search_begin_task(end)
    elif namespace.create:
        create = create_begin_or_end(namespace.create)
        if not create:
            return
        tasks = actions.search_create_task(create)
    elif namespace.priority:
        tasks = actions.search_priority_task(namespace.priority)
    elif namespace.type:
        tasks = actions.search_priority_task(namespace.type)
    if tasks:
        for task in tasks.values():
            print(task)


def show_users():
    actions = Actions(create_storage())
    users = actions.get_all_users()
    for user in users:
        print(user)


def show_categories(namespace):
    actions = Actions(create_storage())
    categories = actions.get_all_categories_name(namespace.user)
    for category in categories:
        print(category)


def print_information_about_task(task):
    """Displays task information on the console"""
    print("ID: ", task.ID)
    print("users: ", task.users)
    print("parent: ", task.parent)
    print("name: ", task.name)
    print("begin: ", task.begin.strftime("%d/%m/%Y %H:%M"))
    print("end: ", task.end.strftime("%d/%m/%Y %H:%M"))
    print("type: ", task.type)
    print("priority: ", task.priority)
    print("status: ", task.status)
    print("tasks: ", task.tasks)
    print("comment: ", task.comment)
    print("relation: ", task.relation)


def print_task(namespace):
    actions = Actions(create_storage())
    task = actions.get_task(namespace.ID)
    if not task:
        sys.stderr.write("In storage has not task with ID: ", namespace.ID + ".\n")
        return
    print_information_about_task(task)


def print_all_tasks():
    actions = Actions(create_storage())
    tasks = actions.get_all_task_from_storage()
    if tasks:
        for task in tasks.values():
            print(task)


def print_all_user_tasks(namespace):
    actions = Actions(create_storage())
    tasks = actions.get_all_tasks(namespace.user)
    if tasks:
        for task in tasks.values():
            print(task)


def print_main_user_tasks(namespace):
    actions = Actions(create_storage())
    tasks = actions.get_main_tasks(namespace.user)
    if tasks:
        for task in tasks.values():
            print(task)


def print_task_subtasks(namespace):
    actions = Actions(create_storage())
    subtasks = actions.get_subtasks(namespace.task)
    if subtasks:
        for task in subtasks.values():
            print(task)


def print_linked_task(namespace):
    actions = Actions(create_storage())
    relations = actions.get_linked_tasks(namespace.task)
    if relations:
        for relation in relations.values():
            print(relation)


def print_name_tasks(namespace):
    actions = Actions(create_storage())
    tasks = actions.search_name_tasks(namespace.name)
    if tasks:
        for task in tasks.values():
            print(task)


def print_sort_task_priority(namespace):
    actions = Actions(create_storage())
    tasks = actions.get_sorted_priority_tasks(namespace.user, namespace.task)
    if tasks:
        for key in tasks.keys():
            print(str(key) + ": ")
            for task in tasks[key]:
                print(task)
            print()


def print_sort_task_type(namespace):
    actions = Actions(create_storage())
    tasks = actions.get_sorted_type_tasks(namespace.user, namespace.task)
    if tasks:
        for key in tasks.keys():
            print(str(key) + ": ")
            for task in tasks[key]:
                print(task)
            print()


def print_day_tasks(namespace):
    date = parse_date(namespace.day)
    if not date:
        sys.stderr.write(namespace.day + " is wrong. Need string 'Days-Months-Years' or 'Days/Months/Years'. "
                                         "E.g. '12-07-2018', '12/07/2018'.\n")
        return
    actions = Actions(create_storage())
    tasks = actions.get_day_tasks(date, namespace.user)
    if tasks:
        for task in tasks.values():
            print(task)


def show_tasks(namespace):
    if not namespace.task and not namespace.user and not namespace.day and not namespace.sort and not namespace.all\
            and not namespace.name:
        print("usage: tracker show tasks [-h] [--user USER] [--all] [--task TASK] [--day DAY]"
              "[--sort {type,priority}]\n\n"
              "optional arguments:\n"
              "  -h, --help            show this help message and exit\n"
              "  --user USER, -u USER  User's name\n"
              "  --all, -a             Outputs not only main tasks (those, that do not have\n"
              "                        subtasks), but all\n"
              "  --task TASK, -t TASK  Task's ID, for which you want to see subtasks\n"
              "  --day DAY, -d DAY     Views tasks of the date DD-MM-YYYY or DD/MM/YYYY\n"
              "  --sort {type,priority}, -s {type,priority} Prints sorted tasks")
    elif namespace.all:
        if not namespace.user:
            print_all_tasks()
        else:
            print("All user's tasks:")
            print_all_user_tasks(namespace)
    elif not namespace.task and not namespace.day and not namespace.sort and not namespace.name:
        print("Main user's task:")
        print_main_user_tasks(namespace)
    elif not namespace.user and not namespace.day and not namespace.sort and not namespace.name:
        print("Task's subtasks:")
        print_task_subtasks(namespace)
    elif namespace.name:
        print("Tasks with name: " + namespace.name)
        print_name_tasks(namespace)
    elif namespace.sort == "priority":
        print("Sorted on priority tasks:")
        print_sort_task_priority(namespace)
    elif namespace.sort == "type":
        print("Sorted on type tasks:")
        print_sort_task_type(namespace)
    elif namespace.day:
        print("Day tasks:")
        print_day_tasks(namespace)


def configure_logging(namespace):
    if not namespace.on and not namespace.off and not namespace.format and not namespace.level and not namespace.path:
        print("usage: tracker logger [-h] [--on] [--off] [--format FORMAT] [--path PATH]"
              "[--level {ERROR,WARNING,CRITICAL,INFO,DEBUG}]\n\n"
              "optional arguments:\n"
              "  -h, --help            show this help message and exit\n"
              "  --on                  Turned on logger\n"
              "  --off                 Turned off logger\n"
              "  --format FORMAT, -f FORMAT format of the log line\n"
              "  --path PATH, -p PATH  path to the file in which the logs are written\n"
              "  --level {ERROR,WARNING,CRITICAL,INFO,DEBUG}, -l {ERROR,WARNING,CRITICAL,INFO,DEBUG} "
              "with what level to log")
    if namespace.on:
        write_logging_enable("True")
    elif namespace.off:
        write_logging_enable("False")
    if namespace.format:
        write_logging_format(namespace.format)
    if namespace.level:
        write_logging_level(namespace.level)
    if namespace.path:
        write_logging_path(namespace.path)


def write_logging_enable(enable):
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    logging_config.set("logging", "enable", enable)
    with open(config_file, "w") as config_file:
        logging_config.write(config_file)


def write_logging_level(level):
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    logging_config.set("logging", "level", level)
    with open(config_file, "w") as config_file:
        logging_config.write(config_file)


def write_logging_path(path):
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    logging_config.set("logging", "path", path)
    with open(config_file, "w") as config_file:
        logging_config.write(config_file)


def write_logging_format(format_):
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    logging_config.set("logging", "format", format_)
    with open(config_file, "w") as config_file:
        logging_config.write(config_file)


def get_logging_enable():
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    enable = logging_config.get("logging", "enable")
    if enable == "True":
        return True
    return False


def get_logging_level():
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    level = logging_config.get("logging", "level")
    if level == "DEBUG":
        return logging.DEBUG
    elif level == "INFO":
        return logging.INFO
    elif level == "WARNING":
        return logging.WARNING
    elif level == "ERROR":
        return logging.ERROR
    elif level == "CRITICAL":
        return logging.CRITICAL


def get_logging_path():
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    return logging_config.get("logging", "path")


def get_logging_format():
    config_file = config.LOGGING_CONF
    logging_config = configparser.ConfigParser()
    logging_config.read(config_file)
    return logging_config.get("logging", "format")


def check():
    """Checks whether there are completed tasks or whether the time has come for notifications"""
    actions = Actions(create_storage())
    actions.check_all_reminder()
    actions.check_all_tasks()
    actions.check_all_plans()


def print_notices():
    for username in Notice.notices.keys():
        print(username, " has notices:")
        for notice in Notice.notices[username]:
            print(notice)
    Notice.remove_all_notices()


def is_valid_file(filename):
    if not os.path.exists(filename):
        sys.stderr.write("The file {0} does not exist!\n".format(filename))
    elif not os.path.isfile(filename):
        sys.stderr.write("{0} does not file!\n".format(filename))
    else:
        return True


def get_path(path):
    if path == "default":
        return "default"
    if is_valid_file(path):
        return path


def change_archive_path(path):
    """Changes the path to the archive if the path was exist."""
    filename = get_path(path)
    if not filename:
        return
    if filename == "default":
        path = config.DEFAULT_ARCHIVE_PATH
    write_archive_file(path)


def change_tasks_path(path):
    """Changes the path to database if the path was exist."""
    filename = get_path(path)
    if not filename:
        return
    if filename == "default":
        path = config.DEFAULT_DATABASE_PATH
    write_database_file(path)


def change_categories_path(path):
    """Changes the path to categories file if the path was exist."""
    filename = get_path(path)
    if not filename:
        return
    if filename == "default":
        path = config.DEFAULT_CATEGORIES_PATH
    write_category_file(path)


def change_relations_path(path):
    """Changes the path to categories file if the path was exist."""
    filename = get_path(path)
    if not filename:
        return
    if filename == "default":
        path = config.DEFAULT_RELATIONS_PATH
    write_relation_file(path)


def write_database_file(path):
    config_file = config.PATHWAYS_CONF
    database_config = configparser.ConfigParser()
    database_config.read(config_file)
    database_config.set("pathways", "tasks", path)
    with open(config_file, "w") as config_file:
        database_config.write(config_file)


def write_archive_file(path):
    config_file = config.PATHWAYS_CONF
    database_config = configparser.ConfigParser()
    database_config.read(config_file)
    database_config.set("pathways", "archive", path)
    with open(config_file, "w") as config_file:
        database_config.write(config_file)


def write_category_file(path):
    config_file = config.PATHWAYS_CONF
    category_config = configparser.ConfigParser()
    category_config.read(config_file)
    category_config.set("pathways", "categories", path)
    with open(config_file, "w") as config_file:
        category_config.write(config_file)


def write_relation_file(path):
    config_file = config.PATHWAYS_CONF
    relation_config = configparser.ConfigParser()
    relation_config.read(config_file)
    relation_config.set("pathways", "relations", path)
    with open(config_file, "w") as config_file:
        relation_config.write(config_file)


def get_database_file():
    config_file = config.PATHWAYS_CONF
    database_config = configparser.ConfigParser()
    database_config.read(config_file)
    return database_config.get("pathways", "tasks")


def get_archive_file():
    config_file = config.PATHWAYS_CONF
    database_config = configparser.ConfigParser()
    database_config.read(config_file)
    return database_config.get("pathways", "archive")


def get_category_file():
    config_file = config.PATHWAYS_CONF
    category_config = configparser.ConfigParser()
    category_config.read(config_file)
    return category_config.get("pathways", "categories")


def get_relations_file():
    config_file = config.PATHWAYS_CONF
    relation_config = configparser.ConfigParser()
    relation_config.read(config_file)
    return relation_config.get("pathways", "relations")
