import unittest
import os
from datetime import datetime, time
import datetime as dt
from task_tracker.entities import (
    Task,
    Reminder,
    Category,
    Notice
)
from task_tracker.actions import Actions
from task_tracker.storage import Storage


class TestsTasks(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        database = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/Tasks.json"
        archive = os.path.dirname(os.path.dirname(__file__)) + "/task_tracker/data/RemovedTasks.json"
        cls.storage = Storage(database, archive)
        cls.actions = Actions(cls.storage)

    def setUp(self):
        task1 = Task(users=["username1"], id_="SVK47GJF", name="task1", type_="homework", comment="comment", priority=3,
                     begin=datetime.now() - dt.timedelta(hours=2), end=datetime.now() - dt.timedelta(hours=1),
                     reminder=Reminder(date=datetime.now(), comment="comment_reminder"), repeat="every 32 months")
        task2 = Task(users=["username1"], name="task2", type_="homework", comment="comment2", priority=5)
        task3 = Task(users=["username1"], priority=4, type_="food")
        subtask1 = Task(users=["username1"], parent=task1.ID, name="subtask1", type_="food")
        subtask2 = Task(users=["username1"], parent=task3.ID, priority=3, type_="homework", repeat="every 1 hours",
                        begin=datetime.now() - dt.timedelta(hours=6),
                        end=datetime.now() - dt.timedelta(hours=5))
        subtask3 = Task(users=["username1", "user"], name="task1", parent=task3.ID, type_="homework", plan="every 1 minutes")
        self.task1 = self.actions.add_new_task(task1)
        self.task2 = self.actions.add_new_task(task2)
        self.task3 = self.actions.add_new_task(task3)
        self.subtask1 = self.actions.add_new_task(subtask1)
        self.subtask2 = self.actions.add_new_task(subtask2)
        self.subtask3 = self.actions.add_new_task(subtask3)

        self.task1 = self.storage.load_task(self.task1.ID)
        self.task3 = self.storage.load_task(self.task3.ID)

        self.actions.check_all_plans()

    def tearDown(self):
        self.storage.load_and_delete_task(self.task1.ID)
        self.storage.load_and_delete_task(self.task2.ID)
        self.storage.load_and_delete_task(self.task3.ID)
        self.storage.load_and_delete_task(self.subtask1.ID)
        self.storage.load_and_delete_task(self.subtask2.ID)
        self.storage.load_and_delete_task(self.subtask3.ID)

    def test_create_task(self):

        self.assertIsInstance(self.task1, Task)
        self.assertIsInstance(self.task2, Task)
        self.assertIsInstance(self.subtask1, Task)
        self.assertIsInstance(self.subtask2, Task)

    def test_add_task(self):

        with self.assertRaisesRegex(ValueError, "Received object is not a task"):
            self.actions.add_new_task("new_task")

        self.assertEqual(self.task1.ID, self.subtask1.parent)
        self.assertEqual(self.task3.tasks, [self.subtask2.ID, self.subtask3.ID])
        self.assertEqual(self.task1.tasks, [self.subtask1.ID])
        self.assertEqual(self.task1.begin.date(), (datetime.now() - dt.timedelta(hours=2)).date())
        self.assertEqual(self.task2.begin, datetime.combine(datetime.now().date(), time(0, 0, 0)))
        self.assertEqual(self.task2.end, datetime(9999, 12, 31, 0, 0))
        self.assertIn("Added new task " + str(self.subtask1), Notice.notices["username1"])

    def test_edit_task(self):
        self.task2.name = "edited_task"
        self.subtask1.name = "edited_subtask"
        task1_id = self.task1.ID
        self.task1.ID = "RTFHO"

        edit1 = self.actions.edit_task(self.task2, "username1")
        edit2 = self.actions.edit_task(self.subtask1, "username1")
        edit3 = self.actions.edit_task(self.task1, "username1")

        with self.assertRaisesRegex(ValueError, "Received object is not a task"):
            self.actions.edit_task("string", "username1")
        with self.assertRaisesRegex(AttributeError, "This user doesn't have access to the task"):
            self.actions.edit_task(self.subtask2, "username3")

        self.assertEqual(self.task2.name, "edited_task")
        self.assertEqual(self.subtask1.name, "edited_subtask")
        self.assertIn("Edited task: " + str(edit1), Notice.notices["username1"])
        self.assertIn("Edited task: " + str(edit2), Notice.notices["username1"])
        self.assertIsNone(edit3)

        self.storage.load_and_delete_task(task1_id)
        self.task2 = self.storage.load_and_delete_task(edit1.ID)
        self.subtask1 = self.storage.load_and_delete_task(edit2.ID)

    def test_add_user_in_task(self):

        subtask1 = self.actions.add_new_user_in_task(self.subtask1.ID, username="username2")
        task3 = self.actions.add_new_user_in_task(self.task3.ID, username="username2")

        self.subtask1 = self.storage.load_task(subtask1.ID)
        self.task3 = self.storage.load_task(task3.ID)

        self.assertIn("username2", self.subtask1.users)
        self.assertIn("username2", self.task3.users)
        self.assertIn("username1", self.subtask1.users)
        self.assertIn("username1", self.task3.users)

        with self.assertRaisesRegex(AttributeError, "This user already has access to the task"):
            self.actions.add_new_user_in_task(self.task3.ID, username="username2")

    def test_remove_user_from_task(self):

        self.subtask3 = self.actions.remove_user_from_task(self.subtask3.ID, username="user")

        self.assertNotIn("user", self.subtask3.users)
        self.assertIn("username1", self.subtask3.users)

        with self.assertRaisesRegex(AttributeError, "This user doesn't have access to the task"):
            self.actions.remove_user_from_task(self.subtask3.ID, username="user")

    def test_remove_task(self):

        task1 = self.actions.add_new_user_in_task(self.task1.ID, username="username2")
        removed = self.actions.remove_task("FJHV46")
        self.task1 = self.actions.remove_task(task1.ID)

        self.assertIn("username1", self.task1.users)
        self.assertIsNone(removed)
        self.assertIn("Removed task: " + str(self.task1), Notice.notices["username2"])
        self.assertIn("Removed task: " + str(self.task1), Notice.notices["username1"])
        self.assertIn(task1.name, self.storage.load_removed_task(self.task1.ID).name)
        self.assertIn(self.subtask1.name, self.storage.load_removed_task(self.subtask1.ID).name)

    def test_get_task(self):

        task3 = self.actions.get_task(self.task3.ID)
        subtask3 = self.actions.get_task(self.subtask3.ID)

        self.assertEqual(task3.name, self.storage.load_task(self.task3.ID).name)
        self.assertEqual(subtask3.name, self.storage.load_task(self.subtask3.ID).name)

    def test_get_tasks(self):

        tasks = self.actions.get_all_tasks("username1").keys()
        main_tasks = self.actions.get_main_tasks("username1").keys()
        main_tasks2 = self.actions.get_main_tasks("username3")
        subtasks = self.actions.get_subtasks(self.task3.ID)

        self.assertIn(self.task1.ID, tasks)
        self.assertIn(self.task2.ID, tasks)
        self.assertIn(self.task3.ID, tasks)
        self.assertIn(self.subtask1.ID, tasks)
        self.assertIn(self.subtask2.ID, tasks)
        self.assertIn(self.subtask3.ID, tasks)

        self.assertIn(self.task1.ID, main_tasks)
        self.assertIn(self.task2.ID, main_tasks)
        self.assertIn(self.task3.ID, main_tasks)
        self.assertNotIn(self.subtask1.ID, main_tasks)
        self.assertNotIn(self.subtask2.ID, main_tasks)
        self.assertNotIn(self.subtask3.ID, main_tasks)

        self.assertEqual(main_tasks2, {})

        self.assertNotIn(self.subtask1.ID, subtasks)
        self.assertIn(self.subtask2.ID, subtasks)
        self.assertIn(self.subtask3.ID, subtasks)

    def test_sorted_priority(self):

        sort_tasks = self.actions.get_sorted_priority_tasks(username="username1")
        sort_subtasks = self.actions.get_sorted_priority_tasks(task_id=self.task3.ID)

        self.assertCountEqual(sort_tasks, {3: [self.task1], 4: [self.task3], 5: [self.task2]})
        self.assertCountEqual(sort_subtasks, {2: [self.subtask3, self.subtask1], 3: [self.subtask2]})

    def test_sorted_type(self):

        sort_tasks = self.actions.get_sorted_type_tasks(username="username1")
        sort_subtasks = self.actions.get_sorted_type_tasks(task_id=self.task3.ID)

        self.assertCountEqual(sort_tasks, {"homework": [self.task1, self.task2], "food": [self.task3]})
        self.assertCountEqual(sort_subtasks, {"homework": [self.subtask2, self.subtask3]})

    def test_day_tasks(self):

        tasks1 = self.actions.get_day_tasks(datetime.now() - dt.timedelta(minutes=90), "username1").keys()
        tasks2 = self.actions.get_day_tasks(datetime(2018, 7, 7, 0, 0), "username1").keys()

        self.assertCountEqual(tasks1, [self.task1.ID, self.task2.ID, self.task3.ID])
        self.assertCountEqual(tasks2, [self.task2.ID, self.task3.ID])

    def test_linked_tasks(self):

        relation1 = self.actions.add_new_link(self.task2.ID, self.task3.ID)
        relation2 = self.actions.add_new_link(self.task2.ID, self.task1.ID)
        tasks = self.actions.get_linked_tasks(self.task2.ID)

        self.assertEqual(relation1.first_task_id, self.task2.ID)
        self.assertEqual(relation2.second_task_id, self.task1.ID)
        self.assertCountEqual(tasks.keys(), [relation1.relation_id, relation2.relation_id])

        with self.assertRaisesRegex(AttributeError, "These tasks are already connected"):
            self.actions.add_new_link(self.task2.ID, self.task3.ID)

        with self.assertRaisesRegex(AttributeError, "The tasks are not related"):
            self.actions.remove_link(self.task1.ID, self.task3.ID)

        self.actions.remove_link(self.task2.ID, self.task3.ID)
        task = self.actions.remove_link(self.task1.ID, "RDKNF")

        self.assertIsNone(self.storage.load_and_delete_relation(relation1.relation_id))
        self.assertIsNotNone(self.storage.load_and_delete_relation(relation2.relation_id))
        self.assertIsNone(task)

    def test_deadlines_task(self):

        deadlines = self.actions.is_deadline_date(datetime.now() + dt.timedelta(days=50), "username1").keys()

        self.assertIn(self.task1.ID, deadlines)
        self.assertNotIn(self.subtask3.ID, deadlines)
        self.assertNotIn(self.task2.ID, deadlines)
        self.assertIn("Task: " + str(self.task1) + " ends now.", Notice.notices["username1"])
        self.assertNotIn("Task: " + str(self.subtask3) + " ends now.", Notice.notices["username1"])

    def test_period_task_hours(self):

        self.actions.period_tasks(datetime.now() + dt.timedelta(hours=2), "username1")

        self.subtask2 = self.storage.load_task(self.subtask2.ID)

        if not self.subtask2.begin.strftime("%Y/%m/%d %H") == (datetime.now() - dt.timedelta(hours=6)).strftime("%Y/%m/%d %H"):
            self.assertEqual(self.subtask2.begin.strftime("%Y/%m/%d %H"), datetime.now().strftime("%Y/%m/%d %H"))
            self.assertEqual(self.subtask2.end.strftime("%Y/%m/%d %H"),
                             (datetime.now() + dt.timedelta(hours=1)).strftime("%Y/%m/%d %H"))

    def test_period_task_month(self):

        self.actions.period_tasks(datetime.now() + dt.timedelta(days=1), "username1")

        self.task1 = self.storage.load_task(self.task1.ID)

        self.assertEqual(self.task1.begin.strftime("%Y/%m/%d %H"),
                         (datetime.now() - dt.timedelta(hours=2)).replace(year=2021, month=2).strftime("%Y/%m/%d %H"))
        self.assertEqual(self.task1.end.strftime("%Y/%m/%d %H"),
                         (datetime.now() - dt.timedelta(hours=1)).replace(year=2021, month=2).strftime("%Y/%m/%d %H"))

    def test_plan_task(self):

        plan_tasks = self.actions.plan_tasks(self.subtask3.ID, date=datetime.now() + dt.timedelta(minutes=4, seconds=30))
        task1 = self.storage.load_and_delete_task(plan_tasks[0])

        self.assertEqual(len(plan_tasks), 4)
        self.assertIsNotNone(task1)
        self.assertEqual(task1.name, self.subtask3.name)
        self.assertIsNotNone(self.storage.load_and_delete_task(plan_tasks[1]))
        self.assertIsNotNone(self.storage.load_and_delete_task(plan_tasks[2]))
        self.assertIsNotNone(self.storage.load_and_delete_task(plan_tasks[3]))

    def test_is_reminder(self):

        self.actions.is_reminder("username1")

        task_reminder = self.storage.load_task(self.task1.ID).reminder

        self.assertIsNone(task_reminder.comment)
        self.assertIsNone(task_reminder.date)
        self.assertIn("Reminder! Task " + str(self.task1) + " begins soon. " + str(self.task1.reminder.comment),
                      Notice.notices["username1"])

    def test_search_name_tasks(self):

        tasks1 = self.actions.search_name_tasks("task1")
        tasks2 = self.actions.search_name_tasks("subtask1")
        tasks3 = self.actions.search_name_tasks("abracadabra")

        self.assertCountEqual(tasks1.keys(), [self.subtask3.ID, self.task1.ID])
        self.assertCountEqual(tasks2.keys(), [self.subtask1.ID])
        self.assertDictEqual(tasks3, {})

    def test_search_begin_tasks(self):

        tasks = self.actions.search_begin_task(datetime.now())

        self.assertIn(self.task1.ID, tasks.keys())
        self.assertIn(self.task2.ID, tasks.keys())
        self.assertIn(self.task3.ID, tasks.keys())
        self.assertIn(self.subtask1.ID, tasks.keys())
        self.assertIn(self.subtask2.ID, tasks.keys())
        self.assertIn(self.subtask3.ID, tasks.keys())

    def test_search_end_tasks(self):

        tasks = self.actions.search_end_task(datetime(9999, 12, 31))

        self.assertIn(self.task2.ID, tasks.keys())
        self.assertIn(self.task3.ID, tasks.keys())
        self.assertIn(self.subtask1.ID, tasks.keys())
        self.assertIn(self.subtask3.ID, tasks.keys())

    def test_get_all_users(self):

        users = self.actions.get_all_users()

        self.assertIn("username1", users)

    def test_get_all_subtasks(self):

        subtask = self.actions.add_new_task(Task(users="username1", parent=self.subtask1.ID))
        subtasks1 = self.actions.get_all_subtasks(self.task1.ID)

        self.assertEqual(subtasks1[0].ID, self.task1.ID)
        self.assertEqual(subtasks1[1].ID, self.subtask1.ID)
        self.assertEqual(subtasks1[2].ID, subtask.ID)

        self.storage.load_and_delete_task(subtask.ID)

    def test_add_category(self):

        category = Category("username1", "task")

        category = self.actions.add_category(category)

        self.assertEqual(self.storage.load_and_delete_category(category.category_id).name, "task")
        with self.assertRaisesRegex(ValueError, "Received object is not Category"):
            self.actions.add_category("category")

    def test_remove_category(self):

        category = Category("username1", "task")
        category = self.actions.add_category(category)
        self.actions.edit_task_fields(self.subtask1.ID, "username1", category_id=category.category_id)

        with self.assertRaisesRegex(AttributeError, "This user doesn't have this category"):
            self.actions.remove_category(category.category_id, "username2")

        self.actions.remove_category(category.category_id, "username1")

        self.assertIsNone(self.storage.load_and_delete_category(category.category_id))
        self.assertIsNone(self.storage.load_task(self.subtask1.ID).category_id)

    def test_edit_category(self):

        category = Category("username1", "task")
        category = self.actions.add_category(category)
        self.actions.edit_task_fields(self.subtask1.ID, "username1", category_id=category.category_id)

        with self.assertRaisesRegex(AttributeError, "This user doesn't have this category"):
            self.actions.edit_category(category.category_id, "username2")

        self.actions.edit_category(category.category_id, "username1", "category_name")

        self.assertEqual(self.storage.load_and_delete_category(category.category_id).name, "category_name")

    def test_get_all_categories(self):

        category = self.actions.add_category(Category("username1", "task"))
        self.actions.edit_task_fields(self.subtask1.ID, "username1", category_id=category.category_id)
        category1 = self.actions.add_category(Category("username1", "bug"))
        self.actions.edit_task_fields(self.subtask2.ID, "username1", category_id=category1.category_id)

        categories_name = self.actions.get_all_categories_name()
        categories_user = self.actions.get_all_categories_name("username1")

        self.assertCountEqual(categories_name, ["task", "bug"])
        self.assertCountEqual(categories_user, ["task", "bug"])

        self.storage.load_and_delete_category(category.category_id)
        self.storage.load_and_delete_category(category1.category_id)


if __name__ == "__main__":
    unittest.main()
