"""Module of main classes used in the program. There are classes Task, Reminder and Notice.

Class Task is designed to describe the tasks of users.
Class Reminder for representing one field in class Task
Class Category for representing one field in class Task.
Class Relations to store links between tasks.
Class Notice for notify users of main changes.

"""

import random
import string
import datetime


class Task:

    """Class Task is designed to describe the tasks of users.

    Class Task has field:
    users - users, who have access to the task (list)
    ID - task's identification number (random string of letters of the Latin alphabet and numbers),
    name - task's name (string),
    tasks - task's subtasks (list of task's ID),
    date_create - date of task creation (datetime),
    date_redact - date task's last editing (datetime),
    repeat - on what rule to repeat the task: every day/week/month/year, on weekdays/weekend (string),
    begin - date task's begin (datetime),
    end - date task's end (datetime),
    type - task's type: work/study/relaxation/sport/food/family/reading/travel/meeting/event (string),
    priority - task's priority: 1, 2, 3, 4 or 5 (int),
    status - task's execution status: wait/process/done/fail (string),
    reminder - when and with what comment to remind about the task (Reminder),
    comment - task's description (string),
    relation - what tasks the current task is related to (list of task's ID),
    parent - ID of parent task (string).
    plan - with this rule create new task (string)
    category_id = Category's ID (string)
    """

    def __init__(self, users, id_=None, name=None, tasks=None, repeat=None, begin=None, end=None, type_=None,
                 status=None, priority=2, reminder=None, comment=None, parent=None, plan=None,
                 category_id=None, date_create=None, date_edit=None, last_create=None):
        """Task's constructor.

        :param users: usernames to which we add the task (list)
        :param id_: ID new or existing tasks - random string of letters of the Latin alphabet and numbers
        :param name: Tasks's name (string)
        :param tasks: Task's subtasks (list of task's ID)
        :param repeat: field for period tasks:'weekends'/'weekdays' or string in format months-weeks-days-hours-minutes,
        :param begin: Task's begin. If task was created without field 'begin',start date is current day 00:00 (datetime)
        :param end: Task's end. If task was created without field 'end', end date is 31.12.9999 (datetime)
        :param type_: Task's type (string)
        :param status: Task's status: 'process'/'done'/'fail'/'wait' (string)
        :param priority: Task's priority: 1, 2, 3, 4, 5 (int)
        :param reminder: Tasks's reminder (Reminder)
        :param comment: Task's comment (string)
        :param parent - ID of parent task (string),
        :param plan - string with the same format as repeat (string)
        """
        if isinstance(users, str):
            self.users = []
            self.users.append(users)
        elif isinstance(users, list):
            self.users = users
        else:
            raise ValueError("Users is not list or string!")
        if not id_:
            self.ID = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        else:
            self.ID = id_
        self.name = name
        self.tasks = tasks
        self.date_create = date_create
        if not date_create:
            self.date_create = datetime.datetime.now()
        self.date_edit = date_edit
        if not date_edit:
            self.date_edit = datetime.datetime.now()
        self.repeat = repeat
        if not begin:
            self.begin = datetime.datetime.combine(datetime.datetime.now().date(), datetime.time(0, 0, 0))
        else:
            self.begin = begin
        if not end:
            self.end = datetime.datetime(9999, 12, 31)
        else:
            self.end = end
        self.type = type_
        self.priority = priority
        self.status = status
        if not reminder:
            self.reminder = Reminder()
        else:
            self.reminder = reminder
        self.comment = comment
        self.parent = parent
        self.plan = plan
        self.last_create = last_create
        if not last_create:
            self.last_create = datetime.datetime.now()
        self.category_id = category_id

    def __str__(self):
        if self.name and self.priority and self.type and self.begin and self.end and self.status:
            return "User - " + str(self.users) + " ID - " + self.ID + " : name - " + self.name + ", priority - " + \
                   str(self.priority) + ", begin: " + self.begin.strftime("%d-%m-%Y %H:%M:%S") + ", end: " +\
                   self.end.strftime("%d-%m-%Y %H:%M:%S") + ", type - " + self.type + ", status: " + self.status
        else:
            return "User - " + str(self.users) + " ID - " + self.ID

    def __repr__(self):
        if self.name and self.priority and self.type:
            return "User - " + str(self.users) + " ID - " + self.ID + " : name - " + self.name + ", priority - " + \
                   str(self.priority) + ", type - " + self.type
        else:
            return "User - " + str(self.users) + " ID - " + self.ID

    @staticmethod
    def create_id():
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))


class Reminder:
    """Class Reminder for representing one field in class Task.

    Class Reminder has fields:
    date - date and time of reminder (datetime),
    comment - reminder's comment (string).

    """

    def __init__(self, date=None, comment=None):
        """Reminder's constructor.

        :param date: Date of the reminder (datetime)
        :param comment: Comment of the reminder (string).
        """

        self.date = date
        self.comment = comment


class Category:
    """One of the possible fields of the Task"""

    def __init__(self, username, name, category_id=None):
        self.username = username
        self.name = name
        self.category_id = category_id
        if not category_id:
            self.category_id = str(random.randint(0, 1000))

    def __str__(self):
        return "User - " + self.username + " ID: " + self.category_id + ", name: " + self.name


class Relations:

    def __init__(self, first_task_id, second_task_id, relation_id=None):
        self.first_task_id = first_task_id
        self.second_task_id = second_task_id
        self.relation_id = relation_id
        if not relation_id:
            self.relation_id = str(random.randint(0, 1000))

    def __str__(self):
        return "ID: " + self.relation_id + " between task: " + self.first_task_id + " and task: " + self.second_task_id


class Notice:
    """Class Notice for notify users of main changes.

    Class Notice has field:
    notices - dict of user's notices {"username1": [notices], "username2": [notices], ...}

    Class Notice has methods:
    add_notice(cls, username, message) - classmethod adds new notice to some user
    remove_all_notices(cls) - staticmethod deletes all notices from all users

    """

    notices = {}

    @classmethod
    def add_notice(cls, username, message):
        """Adds new notice (message) to some user (username)."""
        notices = cls.notices.get(username, [])
        notices.append(message)
        cls.notices[username] = notices

    @classmethod
    def remove_all_notices(cls):
        """Deletes all notices from all users"""
        cls.notices = {}
