"""Module of main class for storing information in the json-files.

There is class Storage, which work with json-files and stores paths to database and archive files.
There are functions task_to_json(task) and task_from_json(task), which help files to work beautifully.

"""

import json
import os
import time
from datetime import datetime
from task_tracker import entities
from task_tracker import logger


class Storage:
    """Class Storage stores paths to database file and archive file.

    Class Storage includes methods:
    load_task(self, task_id) -> task and load_removed_task(self, task_id) -> task - load task or removed task with
            ID=task_id from file,
    save_task(self, task) and save_removed_task(self, task) - save task in json-file,
    load_tasks(self) -> tasks and load_removed_tasks(self) -> tasks - load all tasks from file,
    save_tasks(self, tasks) and save_removed_tasks(self, tasks) - save all tasks in file.

    """

    def __init__(self, database_path=None, archive_path=None, category_path=None, relation_path=None):
        """Class's constructor. Creates class attributes - paths to database and archive files.
        If the paths were not transferred, it takes the default paths.

        :param database_path: path to file with data about tasks,
        :param archive_path: path to archive with removed tasks.
        """

        if not database_path:
            database_path = os.path.dirname(__file__) + "/data/Tasks.json"
        self.database_path = database_path

        if not archive_path:
            archive_path = os.path.dirname(__file__) + "/data/RemovedTasks.json"
        self.archive_path = archive_path

        if not category_path:
            category_path = os.path.dirname(__file__) + "/data/Categories.json"
        self.category_path = category_path

        if not relation_path:
            relation_path = os.path.dirname(__file__) + "/data/Relations.json"
        self.relation_path = relation_path

    def load_and_delete_task(self, task_id):
        """load task with ID=task_id from json-file and delete it from file"""
        id_to_task = self.load_tasks()
        if not id_to_task:
            id_to_task = {}
        task = id_to_task.pop(task_id, None)
        self.save_tasks(id_to_task)
        return task

    def load_task(self, task_id):
        """load task from json-file"""
        id_to_task = self.load_tasks()
        if not id_to_task:
            id_to_task = {}
        return id_to_task.get(task_id, None)

    def save_task(self, task):
        """Saves task in database if task is exist"""
        if not task:
            return
        id_to_task = self.load_tasks()
        id_to_task[task.ID] = task
        self.save_tasks(id_to_task)

    def load_removed_task(self, task_id):
        """load removed task with ID=task_id from json-file"""
        id_to_task = self.load_removed_tasks()
        if not id_to_task:
            id_to_task = {}
        task = id_to_task.pop(task_id, None)
        self.save_removed_tasks(id_to_task)
        return task

    def save_removed_task(self, task):
        """Saves removed task in archive if task is exist"""
        if not task:
            return
        id_to_task = self.load_removed_tasks()
        id_to_task[task.ID] = task
        self.save_removed_tasks(id_to_task)

    def update_task(self, task):
        """Updates info about task"""
        self.load_and_delete_task(task.ID)
        self.save_task(task)

    def load_tasks(self):
        """Loads information about tasks from json-file with path = self.database_path to dict.

        Opens file. If there is no file, it catches an exception and continues to work. Reads
        information about tasks with help function task_from_json(task). Logging actions.

        """
        try:
            if file_is_empty(self.database_path):
                return {}
            with open(self.database_path, 'r') as f:
                return json.load(f, object_hook=task_from_json)
        except FileNotFoundError:
            logger.get_logger().error("File not found")

    def save_tasks(self, tasks):
        """Saves information about tasks from dict to json-file (self.database_file).

        Opens or creates file and writes information about tasks to json-file with help function
        task_to_json(task).

        """
        with open(self.database_path, 'w') as f:
            json.dump(tasks, f, default=task_to_json, ensure_ascii=False, indent=4)

    def load_removed_tasks(self):
        """Loads information about removed tasks from json-file (self.archive_path) to dict."""
        try:
            if file_is_empty(self.archive_path):
                return {}
            with open(self.archive_path, 'r') as f:
                return json.load(f, object_hook=task_from_json)
        except FileNotFoundError:
            logger.get_logger().error("File not found")

    def save_removed_tasks(self, tasks):
        """Saves information about removed tasks from dict to json-file with path = self.archive_path."""
        with open(self.archive_path, 'w') as f:
            json.dump(tasks, f, default=task_to_json, ensure_ascii=False, indent=4)

    def load_and_delete_category(self, category_id):
        categories = self.load_categories()
        if not categories:
            categories = {}
        category = categories.pop(category_id, None)
        self.save_categories(categories)
        return category

    def load_category(self, category_id):
        id_to_category = self.load_categories()
        if not id_to_category:
            id_to_category = {}
        return id_to_category.get(category_id, None)

    def save_category(self, category):
        if not category:
            return
        id_to_category = self.load_categories()
        id_to_category[category.category_id] = category
        self.save_categories(id_to_category)

    def load_categories(self):
        try:
            if file_is_empty(self.category_path):
                return {}
            with open(self.category_path, 'r') as f:
                return json.load(f, object_hook=task_from_json)
        except FileNotFoundError:
            logger.get_logger().error("File not found")

    def save_categories(self, categories):
        with open(self.category_path, 'w') as f:
            json.dump(categories, f, default=task_to_json, ensure_ascii=False, indent=4)

    def update_category(self, category):
        self.load_and_delete_category(category.category_id)
        self.save_category(category)

    def load_relations(self):
        try:
            if file_is_empty(self.relation_path):
                return {}
            with open(self.relation_path, 'r') as f:
                return json.load(f, object_hook=task_from_json)
        except FileNotFoundError:
            logger.get_logger().error("File not found")

    def save_relations(self, relations):
        with open(self.relation_path, 'w') as f:
            json.dump(relations, f, default=task_to_json, ensure_ascii=False, indent=4)

    def load_relation(self, relation_id):
        id_to_relation = self.load_categories()
        if not id_to_relation:
            id_to_relation = {}
        return id_to_relation.get(relation_id, None)

    def save_relation(self, relation):
        if not relation:
            return
        id_to_relation = self.load_relations()
        id_to_relation[relation.relation_id] = relation
        self.save_relations(id_to_relation)

    def load_and_delete_relation(self, relation_id):
        id_to_relation = self.load_relations()
        if not id_to_relation:
            id_to_relation = {}
        relation = id_to_relation.pop(relation_id, None)
        self.save_relations(id_to_relation)
        return relation


def file_is_empty(path):
    """Checks the file for emptiness"""
    return os.stat(path).st_size == 0


JSON_CLASS_FIELD_NAME = '__class__'
JSON_VALUE_FIELD_NAME = '__value__'


def task_to_json(task):
    """Helps to write data to json format"""
    if isinstance(task, time.struct_time):
        return {JSON_CLASS_FIELD_NAME: 'time.asctime', JSON_VALUE_FIELD_NAME: time.asctime(task)}
    if isinstance(task, entities.Task):
        return {JSON_CLASS_FIELD_NAME: 'task', JSON_VALUE_FIELD_NAME: task.__dict__}
    elif isinstance(task, entities.Reminder):
        return {JSON_CLASS_FIELD_NAME: 'reminder', JSON_VALUE_FIELD_NAME: task.__dict__}
    elif isinstance(task, datetime):
        date = [task.year, task.month, task.day, task.hour, task.minute, task.second, task.microsecond]
        return {JSON_CLASS_FIELD_NAME: 'datetime', JSON_VALUE_FIELD_NAME: date}
    elif isinstance(task, entities.Category):
        return {JSON_CLASS_FIELD_NAME: 'category', JSON_VALUE_FIELD_NAME: task.__dict__}
    elif isinstance(task, entities.Relations):
        return {JSON_CLASS_FIELD_NAME: 'relation', JSON_VALUE_FIELD_NAME: task.__dict__}
    raise TypeError(repr(task) + ' is not JSON serializable')


def task_from_json(task):
    """Helps to read data from json format"""
    if JSON_CLASS_FIELD_NAME in task:
        if task[JSON_CLASS_FIELD_NAME] == 'time.asctime':
            return time.strptime(task[JSON_VALUE_FIELD_NAME])
        if task[JSON_CLASS_FIELD_NAME] == 'task':
            return entities.Task(task[JSON_VALUE_FIELD_NAME].get("users"), task[JSON_VALUE_FIELD_NAME].get("ID"),
                                 task[JSON_VALUE_FIELD_NAME].get("name"), task[JSON_VALUE_FIELD_NAME].get("tasks"),
                                 task[JSON_VALUE_FIELD_NAME].get("repeat"), task[JSON_VALUE_FIELD_NAME].get("begin"),
                                 task[JSON_VALUE_FIELD_NAME].get("end"), task[JSON_VALUE_FIELD_NAME].get("type"),
                                 task[JSON_VALUE_FIELD_NAME].get("status"), task[JSON_VALUE_FIELD_NAME].get("priority"),
                                 task[JSON_VALUE_FIELD_NAME].get("reminder"),task[JSON_VALUE_FIELD_NAME].get("comment"),
                                 task[JSON_VALUE_FIELD_NAME].get("parent"), task[JSON_VALUE_FIELD_NAME].get("plan"),
                                 task[JSON_VALUE_FIELD_NAME].get("category_id"),
                                 task[JSON_VALUE_FIELD_NAME].get("date_create"),
                                 task[JSON_VALUE_FIELD_NAME].get("date_edit"),
                                 task[JSON_VALUE_FIELD_NAME].get("last_create"))
        if task[JSON_CLASS_FIELD_NAME] == 'reminder':
            return entities.Reminder(task[JSON_VALUE_FIELD_NAME].get('date'),
                                     task[JSON_VALUE_FIELD_NAME].get('comment'))
        if task[JSON_CLASS_FIELD_NAME] == 'datetime':
            dt_list = task[JSON_VALUE_FIELD_NAME]
            return datetime(dt_list[0], dt_list[1], dt_list[2], dt_list[3], dt_list[4], dt_list[5], dt_list[6])
        if task[JSON_CLASS_FIELD_NAME] == 'category':
            return entities.Category(task[JSON_VALUE_FIELD_NAME].get('username'),
                                     task[JSON_VALUE_FIELD_NAME].get('name'),
                                     task[JSON_VALUE_FIELD_NAME].get('category_id'))
        if task[JSON_CLASS_FIELD_NAME] == 'relation':
            return entities.Relations(task[JSON_VALUE_FIELD_NAME].get('first_task_id'),
                                     task[JSON_VALUE_FIELD_NAME].get('second_task_id'),
                                     task[JSON_VALUE_FIELD_NAME].get('relation_id'))
    return task
