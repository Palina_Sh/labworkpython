"""Module of main functions used in the program.

There are main functions for work with main classes and storage. There is class Action, which contains these functions.
This module includes functions:

add_new_task(new_task) -> new task - adds new task in storage,
remove_task(task_id, username=None) -> removed task - removes task from storage,
remove_all_subtasks(task) - removes all task's subtasks and itself from storage,
edit_task(task, username) -> edited task - edits task,
add_new_user_in_task(task_id, username) -> task - adds new username of task's list of users,
remove_user_from_task(task_id, username) -> task - remove username from task's list of users,
add_new_link(task_id, link) - adds link between two tasks
remove_link(task_id, link) - removes link between two tasks
add_category(category) -> new_category
remove_category(category_id, username=None) -> removed_category
edit_category(category_id, username=None, category_name=None) -> edited category

get_task(task_id) -> task - returns task,
get_all_tasks(username) -> tasks - returns dict of all user's task,
get_main_tasks(usernameh) -> tasks - returns dict of main user's tasks,
get_subtasks(task_id) -> tasks - returns dict of all task's subtasks,
get_all_categories(username=None) - returns all categories

get_linked_tasks(task_id) -> tasks - returns dict of linked tasks,
get_sorted_priority_tasks(username=None, task_id=None) -> tasks - returns sorted by priorities tasks,
get_sorted_type_tasks(username=None, task_id=None) -> tasks - returns sorted by types dict of tasks,
get_day_tasks(date, username) -> tasks - returns tasks of the date,

search_name_task(task_name) -> tasks_dict - searches tasks by name
search_begin_task(task_begin) -> tasks_dict - searches tasks by begin date
search_create_task(task_create) -> tasks_dict - searches tasks by date_create
search_priority_task(priority) -> tasks_dict - searches tasks by priority

compare_date(begin, end) -> bool - comperes dates,
compare_reminder_and_date(reminder) -> bool - comperes reminder's date and date 'now',

is_deadline_date(date_and_time, username) -> tasks - returns dict completed tasks,
period_tasks(date, username) - adds task by the repeat rule,
plan_tasks(task_id, date=None) - create tasks with some plan,
parse_plan_or_repeat(repeat_str) - parse string with repeat or plan rule,

is_reminder(username) - check user's reminders
check_all_reminder() - check reminders of all users,
check_all_tasks() - check deadlines of all tasks.
check_all_plans() - check all tasks and its plans

"""

import datetime
import re
from queue import Queue
from task_tracker import entities
from task_tracker import logger
from task_tracker.storage import Storage


class Actions:
    """Class Action contains main functions, which need for work with tasks.

    Class's constructor takes as an argument the object Storage, in which contain paths to database file and archive
    file. Every methods in this class uses this object. With its help tasks save in database or archive or load from
    these files.

    """

    def __init__(self, init_storage=None, database_path=None, archive_path=None, categories_path=None,
                 relations_path=None):
        """Class's constructor.

        :param init_storage: object Storage, in which stores paths to files,
        :param database_path: path to file with tasks,
        :param archive_path: path to file, in which stores removed tasks.
        """
        if init_storage:
            self.storage = init_storage
        else:
            self.storage = Storage(database_path, archive_path, categories_path, relations_path)

    def add_new_task(self, new_task):
        """Adds new task in storage, logging this action and notifies the user about the added new task.

        :param new_task - task, which you want to add (Task).
        :raise ValueError if the added object is not a task.
        :return new_task.
        """
        if not isinstance(new_task, entities.Task):
            logger.get_logger().error("Trying transfer to function 'add_new_task(new_task)' "
                                      "object different from the task")
            raise ValueError("Received object is not a task")

        if new_task.parent:  # if new task has parent, add new_task.ID in parent's list of tasks
            parent = self.get_task(new_task.parent)
            if not parent.tasks:
                parent.tasks = []
            parent.tasks.append(new_task.ID)
            self.storage.update_task(parent)
        self.storage.save_task(new_task)

        logger.get_logger().info("User: " + str(new_task.users) + " added new task: " + str(new_task))
        entities.Notice.add_notice(new_task.users[0], "Added new task " + str(new_task))
        return new_task

    def remove_task(self, task_id):
        """Removes task and all its subtasks from storage (with status 'fail' if status not 'done'), logging actions
        and notifies users about removed task.

        :param task_id: ID removed task (string).
        :return removed task.
        """
        removed_task = self.get_task(task_id)
        if not removed_task:
            logger.get_logger().error("Task: " + str(removed_task) + " is not found in storage")
            return

        if removed_task.status != 'done':
            removed_task.status = 'fail'

        if removed_task.parent:  # if removed task has parent, remove task.ID from parent's list of tasks
            parent = self.get_task(removed_task.parent)
            parent.tasks.remove(removed_task.ID)
            self.storage.update_task(parent)

        self.remove_all_subtasks(removed_task)  # task and all its subtasks is deleted from storage
        return removed_task

    def remove_all_subtasks(self, task):
        """Removes task and all its subtasks from storage, saves this tasks in archive and logging action.

        :param task: removed task (Task).
        """
        queue = Queue()
        queue.put(task)
        while not queue.empty():
            task = queue.get()
            self.storage.save_removed_task(task)
            logger.get_logger().info("Removed task: " + str(task))
            for user in task.users:
                entities.Notice.add_notice(user, "Removed task: " + str(task))
            if task.tasks:
                for subtask in task.tasks:
                    queue.put(self.storage.load_and_delete_task(subtask))

    def edit_task(self, task, username):
        """Edits task, logging this action and notifies users about edited task.

        :param task: edited task (Task),
        :param username: user, which edits task (string),
        :raise ValueError if the edited object is not a Task,
        :raise AttributeError if username is not in task's list of users.
        :return: edited task.
        """
        if not isinstance(task, entities.Task):
            logger.get_logger().error("Trying transfer to function 'edit_task(task, username)'"
                                      " object different from Task")
            raise ValueError("Received object is not a task")

        edited_task = self.get_task(task.ID)
        if not edited_task:
            logger.get_logger().error("Task: " + str(edited_task) + " is not found in storage.")
            return

        if username not in edited_task.users:
            raise AttributeError("This user doesn't have access to the task")

        self.storage.update_task(task)
        logger.get_logger().info("User - " + username + " edited task: " + str(edited_task))
        entities.Notice.add_notice(username, "Edited task: " + str(edited_task))

        return edited_task

    def edit_task_fields(self, task_id, username, name=None, repeat=None, begin=None, end=None, type_=None, status=None,
                         priority=None, reminder=None, comment=None, parent=None, plan=None, category_id=None):
        """Edits task with ID=task_id, logging this action and notifies users about edited task.

        :param task_id: ID edited task (string)
        :param username: username, which edit this task (string)
        :param name: Tasks's name (string)
        :param repeat: field for period tasks:'weekends'/'weekdays' or string in format months-weeks-days-hours-minutes,
        :param begin: Task's begin. If task was created without field 'begin',start date is current day 00:00 (datetime)
        :param end: Task's end. If task was created without field 'end', end date is 31.12.9999 (datetime)
        :param type_: Task's type (string)
        :param status: Task's status: 'process'/'done'/'fail'/'wait' (string)
        :param priority: Task's priority: 1, 2, 3, 4, 5 (int)
        :param reminder: Tasks's reminder (Reminder)
        :param comment: Task's comment (string)
        :param parent: ID of parent task (string)
        :param plan: with this rule create new tasks (string in format as repeat)
        :param category_id: category's ID, to which the task belongs (string)
        :return: edited task
        """

        edited_task = self.get_task(task_id)
        if not edited_task:
            logger.get_logger().error("Task: " + str(edited_task) + " is not found in storage.")
            return

        if username not in edited_task.users:
            raise AttributeError("This user doesn't have access to the task")

        if name:
            edited_task.name = name
        if repeat:
            edited_task.repeat = repeat
        if begin:
            edited_task.begin = begin
        if end:
            edited_task.end = end
        if type_:
            edited_task.type = type_
        if status:
            edited_task.status = status
        if priority:
            edited_task.priority = priority
        if reminder:
            edited_task.reminder = reminder
        if comment:
            edited_task.comment = comment
        if plan:
            edited_task.plan = plan
        if category_id:
            edited_task.category_id = category_id
        if parent:
            new_parent = self.get_task(parent)
            if not new_parent.tasks:
                new_parent.tasks = []
            new_parent.tasks.append(task_id)
            self.storage.update_task(new_parent)

            old_parent = self.get_task(edited_task.parent)
            old_parent.tasks.remove(task_id)
            self.storage.update_task(old_parent)

            edited_task.parent = parent

        self.storage.update_task(edited_task)
        logger.get_logger().info("User - " + username + " edited task: " + str(edited_task))
        entities.Notice.add_notice(username, "Edited task: " + str(edited_task))

        return edited_task

    def add_new_user_in_task(self, task_id, username):
        """Adds new username in task's list of users, logging this action and notifies user about new task.

        :param task_id: ID task, in which add new user (string),
        :param username: user's name, which add new task (string).
        :raise AttributeError if username is already in task's list of users.
        :return: task.
        """
        task = self.get_task(task_id)
        if not task:
            logger.get_logger().error("Task: " + str(task) + " is not found in storage.")
            return

        if username in task.users:
            raise AttributeError("This user already has access to the task")
        task.users.append(username)
        self.storage.update_task(task)

        logger.get_logger().info("User - " + username + " got task: " + str(task))
        entities.Notice.add_notice(username, "You get new task " + str(task))
        return task

    def remove_user_from_task(self, task_id, username):
        """Removes username from task's users, logging this action and notifies user about new task.

        :param task_id: ID task, from which remove user (string),
        :param username: user's name, which remove from this task (string).
        :raise AttributeError if username is not in task's list of users.
        :return: task.
        """
        task = self.get_task(task_id)
        if not task:
            logger.get_logger().error("Task: " + str(task) + " is not found in storage.")
            return

        if username not in task.users:
            raise AttributeError("This user doesn't have access to the task")

        task.users.remove(username)
        if not task.users:
            self.remove_task(task_id)

        self.storage.update_task(task)

        logger.get_logger().info("User - " + username + " now does not have access to task: " + str(task))
        entities.Notice.add_notice(username, "You now does not have access to task: " + str(task))
        return task

    def add_new_link(self, first_task_id, second_task_id):
        """Adds link between two tasks and logging this action.

        :param first_task_id: ID of first task (string),
        :param second_task_id: ID of second task (string).
        :raise AttributeError if these tasks already were linked.
        """
        first_task = self.get_task(first_task_id)
        second_task = self.get_task(second_task_id)

        if not first_task:
            logger.get_logger().error("Task: " + str(first_task) + " is not found in storage.")
            return
        if not second_task:
            logger.get_logger().error("Task: " + str(second_task) + " is not found in storage.")
            return

        if self.search_link(first_task_id, second_task_id):
            logger.get_logger().error("Trying to link related tasks: " + str(first_task) + ", " + str(second_task))
            raise AttributeError("These tasks are already connected")

        relation = entities.Relations(first_task_id, second_task_id)
        self.storage.save_relation(relation)
        logger.get_logger().info("Task: " + str(first_task) + " and task: " + str(second_task) + " were linked.")
        return relation

    def remove_link(self, first_task_id, second_task_id):
        """Delete link between two tasks and logging this action.

        :param first_task_id: ID of first task (string),
        :param second_task_id: ID of second task (string).
        :raise AttributeError if these tasks are not related.
        """
        first_task = self.get_task(first_task_id)
        second_task = self.get_task(second_task_id)

        if not first_task:
            logger.get_logger().error("Task: " + str(first_task) + " is not found in storage.")
            return
        if not second_task:
            logger.get_logger().error("Task: " + str(second_task) + " is not found in storage.")
            return

        relation = self.search_link(first_task_id, second_task_id)
        if not relation:
            logger.get_logger().error("Trying to remove link between unrelated tasks: " +
                                      str(first_task) + ", " + str(second_task))
            raise AttributeError("The tasks are not related")
        relation = self.storage.load_and_delete_relation(relation.relation_id)
        logger.get_logger().info("Between task: " + str(first_task) + " and task: " + str(second_task) +
                                 " broken connection.")
        return relation

    def add_category(self, category):
        """Adds category in storage"""

        if not isinstance(category, entities.Category):
            logger.get_logger().error("Trying transfer to function 'add_category(category)'"
                                      " object different from Category")
            raise ValueError("Received object is not Category")

        self.storage.save_category(category)
        logger.get_logger().info("Add new category " + str(category))
        return category

    def remove_category(self, category_id, username=None):
        """Removing category with ID=category_id from storage and from all tasks with this category"""

        removed_category = self.storage.load_category(category_id)
        if not removed_category:
            logger.get_logger().error("Category - " + str(removed_category) + " is not found in storage")
            return

        if username and username != removed_category.username:
            raise AttributeError("This user doesn't have this category")

        tasks = self.search_category_task(category_id)
        for task in tasks.values():
            task.category_id = None
            self.storage.update_task(task)
        self.storage.load_and_delete_category(removed_category.category_id)
        logger.get_logger().info("Removed category " + str(removed_category))
        return removed_category

    def edit_category(self, category_id, username=None, category_name=None):
        """Editing category"""

        edited_category = self.storage.load_category(category_id)
        if not edited_category:
            logger.get_logger().error("Category - " + str(edited_category) + " is not found in storage")
            return

        if username and username != edited_category.username:
            raise AttributeError("This user doesn't have this category")

        if category_name:
            edited_category.name = category_name
        self.storage.update_category(edited_category)
        logger.get_logger().info("Edited category " + str(edited_category))
        return edited_category

    def get_all_categories_name(self, username=None):
        """Returns all categories from storage or all user's categories"""
        categories = self.storage.load_categories()
        categories_name = set()
        for category in categories.values():
            if username and category.username == username:
                categories_name.add(category.name)
            elif not username:
                categories_name.add(category.name)
        return list(categories_name)

    def get_task(self, task_id):
        """Returns task from storage with ID=task_id or None if this task isn't in storage.

        :param task_id: ID of the returned task (string).
        """
        id_to_task = self.storage.load_tasks()
        return id_to_task.get(task_id, None)

    def get_all_task_from_storage(self):
        """Returns all tasks from storage"""
        return self.storage.load_tasks()

    def get_all_tasks(self, username):
        """Returns dict of all user's tasks from storage.

        :param username: user ID for which you want to get the tasks (string).
        """
        id_to_task = self.storage.load_tasks()
        user_tasks_dict = {}
        for task in id_to_task.values():
            if username in task.users:
                user_tasks_dict[task.ID] = task
        return user_tasks_dict

    def get_main_tasks(self, username):
        """Returns dict of main user's tasks from storage. Main tasks - tasks, which haven't parent.

        :param username: user ID, for which you want to get main tasks (string).
        """
        id_to_task = self.get_all_tasks(username)
        main_tasks_dict = {}
        for task in id_to_task.values():
            if not task.parent:
                main_tasks_dict[task.ID] = task
        return main_tasks_dict

    def get_subtasks(self, task_id):
        """Returns dict of task's subtasks from storage.

        :param task_id: task ID for which you want to get the subtasks (string).
        """
        id_to_task = self.storage.load_tasks()
        subtasks_dict = {}
        if id_to_task.get(task_id, None):
            for subtask_id in id_to_task[task_id].tasks:
                subtasks_dict[subtask_id] = id_to_task[subtask_id]
        return subtasks_dict

    def get_all_subtasks(self, task_id):
        """Returns dict of all task's subtasks - list of subtasks from storage."""
        task = self.get_task(task_id)
        if not task:
            return None
        queue_subtasks = Queue()
        queue_subtasks.put(task)
        subtasks = [task]
        self.bfs(queue_subtasks, subtasks)
        return subtasks

    def bfs(self, queue, tasks):
        """Breadth-first search for all subtask"""
        if queue.empty():
            return
        task = queue.get()
        if not task.tasks:
            return
        for task_id in task.tasks:
            subtask = self.get_task(task_id)
            tasks.append(subtask)
            queue.put(subtask)
        self.bfs(queue, tasks)

    def get_linked_tasks(self, task_id):
        """Returns chain (dict) linked tasks with current task.

        :param task_id: ID of task, for which you want to find chain linked tasks (string).
        """
        relations_dict = {}
        id_to_relation = self.storage.load_relations()
        for relation in id_to_relation.values():
            if relation.first_task_id == task_id or relation.second_task_id == task_id:
                relations_dict[relation.relation_id] = relation
        return relations_dict

    def get_sorted_priority_tasks(self, username=None, task_id=None):
        """Returns sorted by priorities dict of tasks ({1: [tasks], 2:[tasks], ..., 5: [tasks]}).

        :param username: user ID, for which you want to get sorted tasks (string),
        :param task_id: task ID, for which you want to get sorted subtasks (string).
        """
        tasks_dict = None
        if username:
            tasks_dict = self.get_main_tasks(username)
        elif task_id:
            tasks_dict = self.get_subtasks(task_id)

        if tasks_dict:
            tasks = list(tasks_dict.values())
            sort_task = {}
            for task in tasks:
                priority = sort_task.get(task.priority, [])
                priority.append(task)
                sort_task[task.priority] = priority
            return sort_task

    def get_sorted_type_tasks(self, username=None, task_id=None):
        """Returns sorted by types dict of tasks ({"food": [tasks], "homework": [tasks], ...}).

        :param username: user ID, for which you want to get sorted tasks (string),
        :param task_id: task ID, for which you want to get sorted subtasks (string).
        """
        tasks_dict = None
        if username:
            tasks_dict = self.get_main_tasks(username)
        elif task_id:
            tasks_dict = self.get_subtasks(task_id)

        if tasks_dict:
            tasks = list(tasks_dict.values())
            sort_task = {}
            for task in tasks:
                type_ = sort_task.get(task.type, [])
                type_.append(task)
                sort_task[task.type] = type_
            return sort_task

    def get_day_tasks(self, date, username):
        """Returns dict of tasks of the date.

        :param date: desired date (datetime),
        :param username: user ID, for which you want to get tasks (string).
        """
        user_tasks_dict = self.get_main_tasks(username)
        day_tasks_dict = {}
        for task in user_tasks_dict.values():
            if task.begin and task.begin <= date and task.end and task.end >= date:
                day_tasks_dict[task.ID] = task
        return day_tasks_dict

    def is_deadline_date(self, date_and_time, username):
        """Returns dict completed tasks and notifies user about ending task.

        :param date_and_time: desired datetime (datetime),
        :param username: user ID, for which you want to get tasks (string).
        """
        deadline_tasks_dict = {}
        for task in self.get_all_tasks(username).values():
            if task.end and task.end <= date_and_time:
                deadline_tasks_dict[task.ID] = task
                entities.Notice.add_notice(username, "Task: " + str(task) + " ends now.")
        return deadline_tasks_dict

    def period_tasks(self, date, username):
        """Adds task by the repeat rule (every minute/day/hour/week/month//weekends/weekdays).
        Depending on the rule, it overwrites the start and end dates of the task

        :param date: desired date (datetime),
        :param username: user ID, for which you want to adds period tasks (string).
        """
        deadline_tasks_dict = self.is_deadline_date(date, username)  # Gets tasks that have ended
        if not deadline_tasks_dict:
            return
        for task in deadline_tasks_dict.values():
            task = self.get_task(task.ID)
            new_task = None  # There is task with new start and end dates
            if not task.repeat:
                if task.status != "done":
                    task.status = "fail"  # task is removed from storage with status 'fail'
                self.storage.update_task(task)
                self.remove_task(task.ID)
                continue

            repeat = parse_plan_or_repeat(task.repeat)
            # Change start and end dates according to the rule
            while True:  # While task's end is later than the current date
                if task.end > datetime.datetime.now():
                    break
                if repeat == "weekdays":
                    new_task = weekdays(task)
                elif repeat == "weekend":
                    new_task = weekends(task)
                else:  # Gets start and end dates on rule months-weeks-days-hours-minutes
                    times_repeat = repeat.split("-")
                    new_task = get_every_months(task, times_repeat[0])
                    new_task = get_every_weeks(new_task, times_repeat[1])
                    new_task = get_every_days(new_task, times_repeat[2])
                    new_task = get_every_hours(new_task, times_repeat[3])
                    new_task = get_every_minutes(new_task, times_repeat[4])

            if not task.parent:
                self.storage.update_task(new_task)
            else:
                parent = self.get_task(task.parent)
                if parent.end < task.end:  # If end date of task goes beyond end date of parent - don't create new task
                    self.storage.update_task(task)
                else:
                    self.storage.update_task(new_task)

    def plan_tasks(self, task_id, date=None):
        """Creates the same tasks according to the specified plan. Plan can be 'every 1 minutes' or 'every 3 months' or
        combines all: 'every 2 months 3 weeks 4 days 1 hours 52 minutes'. If task hasn't plan, it returns None.

        :param task_id: task's ID, which must be create by plan (string),
        :param date: desired date (datetime).
        :return: list of ID new tasks.
        """
        task = self.storage.load_task(task_id)
        if not task:
            return

        if not task.plan:
            return

        if not date:
            date = datetime.datetime.now()

        new_tasks = []
        plan = parse_plan_or_repeat(task.plan)
        if not plan:
            return
        last_create = task.last_create
        times_repeat = plan.split("-")
        while True:
            year = int(times_repeat[0]) // 12
            month = int(times_repeat[0]) - year * 12
            year += last_create.year
            if last_create.month + month > 12:
                year += 1
                month = last_create.month + month - 12
            if month == 0:
                month = last_create.month

            last_create += datetime.timedelta(minutes=int(times_repeat[4]))
            last_create += datetime.timedelta(hours=int(times_repeat[3]))
            last_create += datetime.timedelta(days=int(times_repeat[2]))
            last_create += datetime.timedelta(weeks=int(times_repeat[1]))
            last_create = last_create.replace(year=year, month=month)

            if last_create > date:
                break
            task.last_create = last_create
            new_task = entities.Task(users=task.users, name=task.name, id_=entities.Task.create_id(), tasks=task.tasks,
                                     repeat=task.repeat, begin=task.begin, end=task.end, type_=task.type,
                                     status=task.status, priority=task.priority, reminder=task.reminder,
                                     comment=task.comment, parent=task.parent, plan=None)
            new_tasks.append(self.add_new_task(new_task).ID)

        self.storage.update_task(task)
        return new_tasks

    def is_reminder(self, username):
        """Check user's reminders and notifies user's about starting tasks.

        :param username: user ID, which checks for reminders about each task (string).
        """
        id_to_task = self.get_all_tasks(username)
        for task in id_to_task.values():
            if compare_reminder_and_date(task.reminder.date):
                for user in task.users:
                    entities.Notice.add_notice(user, "Reminder! Task " + str(task) + " begins soon. "
                                               + str(task.reminder.comment))
                self.get_task(task.ID)
                task.reminder = entities.Reminder()
                self.storage.update_task(task)

    def check_all_reminder(self):
        """Check reminders of all users"""
        id_to_task = self.storage.load_tasks()
        for task in id_to_task.values():
            self.is_reminder(task.users[0])

    def check_all_tasks(self):
        """Check all deadlines task for each user"""
        id_to_task = self.storage.load_tasks()
        for task in id_to_task.values():
            self.period_tasks(datetime.datetime.now(), task.users[0])

    def check_all_plans(self):
        """Check all tasks and create new tasks on plans"""
        id_to_task = self.storage.load_tasks()
        for task in id_to_task.values():
            self.plan_tasks(task.ID, datetime.datetime.now())

    def search_name_tasks(self, task_name):
        """Searches tasks by name."""
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.name == task_name:
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_begin_task(self, task_begin):
        """Searches tasks by begin date"""
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.begin.date() == task_begin.date():
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_end_task(self, task_end):
        """Searches tasks by end date"""
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.end.date() == task_end.date():
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_create_task(self, task_create):
        """Searches tasks by create date"""
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.date_create.date() == task_create.date():
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_priority_task(self, task_priority):
        """Searches tasks by priority"""
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.priority == task_priority:
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_type_task(self, task_type):
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.priority == task_type:
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_category_task(self, category_id):
        id_to_task = self.storage.load_tasks()
        searched_tasks_dict = {}
        for task in id_to_task.values():
            if task.category_id == category_id:
                searched_tasks_dict[task.ID] = task
        return searched_tasks_dict

    def search_link(self, first_task_id, second_task_id):
        id_to_relation = self.storage.load_relations()
        for relation in id_to_relation.values():
            if relation.first_task_id == first_task_id and relation.second_task_id == second_task_id or\
                    relation.second_task_id == first_task_id and relation.first_task_id == second_task_id:
                return relation

    def get_all_users(self):
        id_to_task = self.storage.load_tasks()
        all_users = []
        for task in id_to_task.values():
            for user in task.users:
                if user not in all_users:
                    all_users.append(user)
        return all_users


def parse_plan_or_repeat(repeat_str):
    """Parse plan or the rule repetition rule string.

    :param repeat_str: the string you want to parse ('every TIME months TIME weeks TIME days TIME hours TIME minutes')
    :return: string in format (months-weeks-days-hours-minutes / weekdays / weekends)
    """
    words_repeat_str = repeat_str.split()
    if len(words_repeat_str) >= 3 and words_repeat_str[0] == "every":
        dict_repeat = {"months": 0, "weeks": 0, "days": 0, "hours": 0, "minutes": 0}
        time = 0
        for i in range(1, len(words_repeat_str)):
            if words_repeat_str[i] in dict_repeat.keys():
                dict_repeat[words_repeat_str[i]] += time
            elif re.match(r"^[0-9]+$", words_repeat_str[i]):  # Whether the string is an integer
                time = int(words_repeat_str[i])
            else:
                break
        else:  # Creates string months-weeks-days-hours-minutes
            return str(dict_repeat["months"]) + "-" + str(dict_repeat["weeks"]) + "-" + str(dict_repeat["days"]) + \
                   "-" + str(dict_repeat["hours"]) + "-" + str(dict_repeat["minutes"])
    elif len(words_repeat_str) == 1 and (words_repeat_str[0] == "weekends" or words_repeat_str[0] == "weekdays"):
        return words_repeat_str[0]


def get_every_minutes(task, time):
    """Rule for adding a task on every few (time) minutes."""
    task.begin += datetime.timedelta(minutes=int(time))
    task.end += datetime.timedelta(minutes=int(time))
    return task


def get_every_hours(task, time):
    """Rule for adding a task on every few (time) hours."""
    task.begin += datetime.timedelta(hours=int(time))
    task.end += datetime.timedelta(hours=int(time))
    return task


def get_every_days(task, time):
    """Rule for adding a task on every few (time) days."""
    task.begin += datetime.timedelta(days=int(time))
    task.end += datetime.timedelta(days=int(time))
    return task


def get_every_weeks(task, time):
    """Rule for adding a task on every few (time) weeks."""
    task.begin += datetime.timedelta(weeks=int(time))
    task.end += datetime.timedelta(weeks=int(time))
    return task


def get_every_months(task, time):
    """Rule for adding a task on every few (time) month."""
    years = [int(time) // 12, int(time) // 12]
    months = [int(time) - years[0]*12, int(time) - years[1]*12]
    task_begin_and_end_month = [task.begin.month, task.end.month]
    years[0] += task.begin.year
    years[1] += task.end.year

    for i in range(len(task_begin_and_end_month)):
        if task_begin_and_end_month[i] + months[i] > 12:
            years[i] += 1
            months[i] = task_begin_and_end_month[i] + months[i] - 12
        if months[i] == 0:
            months[i] = task_begin_and_end_month[i]

    task.begin = task.begin.replace(year=years[0], month=months[0])
    task.end = task.end.replace(year=years[1], month=months[1])
    return task


def weekends(task):
    """Rule for adding a task on weekends."""
    if task.end.weekday() == 5:
        return get_every_days(task, "1")
    elif task.end.weekday() == 6:
        task.begin += datetime.timedelta(days=6)
        task.end += datetime.timedelta(days=6)
        return task


def weekdays(task):
    """Rule for adding a task on weekdays."""
    if 0 <= task.end.weekday() < 4:
        return get_every_days(task, "1")
    elif task.end.weekday() == 4:
        task.begin += datetime.timedelta(days=2)
        task.end += datetime.timedelta(days=2)
        return task


def compare_reminder_and_date(reminder):
    """Comperes reminder's date and date 'now'"""
    if reminder and reminder <= datetime.datetime.now():
        return True
    return False


def compare_date(begin, end):
    """Comperes dates 'begin' and 'end'"""
    if begin and end:
        if begin < end:
            return True
        else:
            return False
    return True
