"""Module includes functions for work with logging.

create_logger(enable, log_level, logging_path, logging_format) - creates logger with preset settings.
get_logger() - returns logger.

"""

import os
import logging


def create_logger(enable=True, log_level=logging.INFO, logging_path=None,
                  logging_format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"):
    """Gets the logger by name, sets the logging settings: level, file, format, and turns it on or off.

    :param enable: the logger is on or off (bool, default=True)
    :param log_level: with what level to log: 'DEBUG'/'INFO'/WARNING'/'ERROR'/'CRITICAL' (string, default="INFO")
    :param logging_path: path to the file in which the logs are written (string, default="./mylogging/my_logging.log")
    :param logging_format: format of the log line (string, default="time - name - level - message")
    """
    # если logging_path==None ззаполнить и убрать выбор уровней логирования. передвать сразу logging.info?
    logger = get_logger()
    if logger.hasHandlers():
        logger.handlers.clear()

    logger.setLevel(log_level)

    if not logging_path:
        logging_path = os.path.dirname(__file__) + "/mylogging/my_logging.log"
    file_handler = logging.FileHandler(logging_path)

    formatter = logging.Formatter(logging_format)
    file_handler.setFormatter(formatter)

    if not enable:
        logger.disabled = True
        return

    logger.disabled = False
    logger.addHandler(file_handler)


def get_logger():
    """Returns logger with name 'task_tracker'"""
    return logging.getLogger("task_tracker")
