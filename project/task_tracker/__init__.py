"""Tasks tracker.

The package is designed to work with tasks. Allows you to create, add, delete, edit, save and return tasks from storage.
Allows you to create subtasks, join tasks. All actions and errors in the program are logged, and users can receive
notification of changes.


CREATE REMINDER:
    >>> from task_tracker import entities
    >>> from datetime import datetime
    >>> reminder = entities.Reminder(date=datetime(2018, 11, 11), comment="my reminder")

CREATE TASK:
    >>> from task_tracker import entities
    >>> task = entities.Task(users=["user"], name="task", type_="work", comment="this is my first task")
    >>> task1 = entities.Task(users="user", name="task1", repeat="weekends", begin=datetime.now(), type_="study",
    ... priority=3, reminder=reminder, comment="my task1", parent=task.ID)

ADD NEW TASK:
    >>> from task_tracker.actions import Actions
    >>> from task_tracker.storage import Storage
    >>> my_storage = Storage(database_path="~/file", archive_path="~/file1")
    >>> my_actions = Actions(my_storage)
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - ZPTHZBVJ : name - task, priority - 2, type - work
    >>> my_actions.add_new_task(task1)
    User - ['user'] ID - WDXVLYIO : name - task1, priority - 3, type - study
    >>> my_actions.add_new_task("task")
    Trying transfer to function 'add_new_task(new_task)' object different from the task
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "~/task_tracker/actions.py", line 57, in add_new_task
        raise ValueError("Received object is not a task")
    ValueError: Received object is not a task

ADD SUBTASK:
    >>> from task_tracker.actions import Actions
    >>> from task_tracker.storage import Storage
    >>> from task_tracker import entities
    >>> task = entities.Task(users=["user"], name="task", type_="work", comment="this is my first task")
    >>> task1 = entities.Task(users="user", name="subtask", repeat="weekends", begin=datetime.now(), type_="study",
    ... priority=3, parent=task.ID)
    >>> my_storage = Storage(database_path="~/file", archive_path="~/file1")
    >>> my_actions = Actions(my_storage)
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - ZPTHZBVJ : name - task, priority - 2, type - work
    >>> my_actions.add_new_task(task1)
    User - ['user'] ID - WDXVLYIO : name - subtask, priority - 3, type - study
    >>> task = my_actions.get_task("ZPTHZBVJ")
    >>> task.tasks
    ['WDXVLYIO']

REMOVE TASK:
    # the task, the subtask, my_storage and my_actions from the previous example
    >>> my_actions.remove_task("ZPTHZBVJ", "user")
    User - ['user'] ID - ZPTHZBVJ : name - task1, priority - 2, type - work
    >>> subtask = my_actions.get_task("WDXVLYIO")
    >>> print(subtask)  # the subtask is also deleted
    None
    >>> print(my_actions.remove_task("task", "user"))
    Task: None is not found in storage
    None

EDIT TASK:
    # my_storage and my_actions from the previous example
    >>> task = entities.Task(users=["user"], name="task", type_="work", comment="this is my second task")
    >>> edited_task = entities.Task(users=["user"], id_=task.ID, name="edited_task", type_="study", comment="edited_task")
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - BY09G79M : name - task, priority - 2, type - work
    >>> my_actions.edit_task(edited_task, "user")
    User - ['user'] ID - BY09G79M : name - task, priority - 2, type - work
    >>> my_actions.get_task(task.ID)
    User - ['user'] ID - BY09G79M : name - edited_task, priority - 2, type - study

ADD USER IN TASK:
    # my_storage and my_actions from the previous example
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - BY09G79M : name - task, priority - 2, type - work
    >>> my_actions.add_new_user_in_task(task.ID, "username")
    User - ['user', 'username'] ID - BY09G79M : name - task, priority - 2, type - work
    >>> my_actions.add_new_user_in_task(task.ID, "username")
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "~/task_tracker/actions.py", line 175, in add_new_user_in_task
        raise AttributeError("This user already has access to the task")
    AttributeError: This user already has access to the task

REMOVE USER FROM TASK:
    # task, my_storage and my_actions from the previous example
    >>> my_actions.remove_user_from_task(task.ID, "user")
    User - ['username'] ID - BY09G79M : name - task, priority - 2, type - work
    >>> my_actions.remove_user_from_task(task.ID, "user")
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "~/task_tracker/actions.py", line 198, in remove_user_from_task
        raise AttributeError("This user doesn't have access to the task")
    AttributeError: This user doesn't have access to the task

    ADD LINK BETWEEN TASK:
    # my_storage and my_actions from the previous example
    >>> task = entities.Task(users=["user"], name="task", type_="work")
    >>> task1 = entities.Task(users=["user"], name="task1", type_="work")
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - M7VKPLMC : name - task, priority - 2, type - work
    >>> my_actions.add_new_task(task1)
    User - ['user'] ID - 0MJ473MW : name - task1, priority - 2, type - work
    >>> my_actions.add_new_link(task.ID, task1.ID)
    User - ['user'] ID - M7VKPLMC : name - task, priority - 2, type - work
    >>> task_with_relation = my_actions.add_new_link(task.ID, task1.ID)
    Trying to link related tasks: User - ['user'] ID - M7VKPLMC, User - ['user'] ID - 0MJ473MW
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "~/task_tracker/actions.py", line 238, in add_new_link
        raise AttributeError("These tasks are already connected")
    AttributeError: These tasks are already connected

REMOVE LINK BETWEEN TASK:
    # task, task1, my_storage and my_actions from the previous example
    >>> my_actions.remove_link(task.ID, task1.ID)
    User - ['user'] ID - M7VKPLMC : name - task, priority - 2, type - work
    >>> my_actions.remove_link(task.ID, task1.ID)
    Trying to remove link between unrelated tasks: User - ['user'] ID - M7VKPLMC, User - ['user'] ID - 0MJ473MW
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "~/task_tracker/actions.py", line 272, in remove_link
        raise AttributeError("The tasks are not related")
    AttributeError: The tasks are not related

CREATE LOGGER:
    # task, my_storage and my_actions from the previous example
    >>> from task_tracker import logger
    >>> logger.create_logger()
    >>> my_actions.add_new_task(task)
    User - ['user'] ID - M7VKPLMC : name - task, priority - 2, type - work
    >>> with open("../mylogging/my_logging.log", 'r') as file:
    ...     file.read()
    "2018-06-13 12:11:06,409 - logic - INFO - User: ['user'] added new task: User - ['user'] ID - M7VKPLMC\n"

Package contains modules:
entities - Module of main classes used in the program. There are classes Task, Reminder and Notice.
actions - Module of main functions used in the program.
storage - Module of main class for storing information in the json-files.
logger - Module includes functions for work with logging.

"""
__all__ = ["entities", "storage", "actions", "logger"]