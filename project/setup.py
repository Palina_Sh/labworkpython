from setuptools import setup, find_packages
from os.path import join, dirname, exists, isfile
from distutils.cmd import Command
from command_line import config, handler


def is_valid_file(filename):
    if not exists(filename):
        print("The file %s does not exist!" % filename)
    if not isfile(filename):
        print("%s does not file!" % filename)
    else:
        return True


def get_path(parser):
    if parser == "default":
        return "default"
    if is_valid_file(parser):
        return parser
    else:
        return get_path(input("Please try again.\n"))


class UserPathsCommand(Command):
    description = "Set your own logging and database paths or choose default"
    user_options = [
        # The format is (long option, short option, description).
        ('ok', None, 'changing logging paths and database'),
    ]

    def initialize_options(self):
        self.ok = None

    def finalize_options(self):
        pass

    def run(self):

        parser = input("\nInput path for tasks (or default: " + config.DEFAULT_DATABASE_PATH + ")\n")
        path = get_path(parser)
        if path == "default":
            path = config.DEFAULT_DATABASE_PATH
        handler.write_database_file(path)

        parser = input("\nInput path for archive (or default: " + config.DEFAULT_ARCHIVE_PATH + ")\n")
        path = get_path(parser)
        if path == "default":
            path = config.DEFAULT_ARCHIVE_PATH
        handler.write_archive_file(path)

setup(
    name='tracker',
    version='1.0',
    packages=find_packages(),
    cmdclass={'set_paths': UserPathsCommand, },
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={'console_scripts': ['tracker = command_line.main:main']},
    include_package_data=True,
    install_requires=["argcomplete"],
    test_suite='tests.tests_for_logic'
)
